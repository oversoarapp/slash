package com.oversoar.slash

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.oversoar.slash.Adapter.TypeRightAdapter
import com.oversoar.slash.Adapter.TypeLeftAdapter
import com.oversoar.slash.Commom.type1
import com.oversoar.slash.Commom.type2
import com.oversoar.slash.Commom.type3
import com.oversoar.slash.Model.FindModel
import com.oversoar.slash.Model.TypeAreaModel
import com.oversoar.slash.Model.TypeModel
import kotlinx.android.synthetic.main.fragment_type.*

class TypeActivity : AppCompatActivity() {

    private var from = ""
    private var type: String? = null
    private var data: String? = null
    private lateinit var leftClick:(Int,String)->Unit
    private lateinit var rightClick:(Int,String)->Unit
    private var leftAdapter: TypeLeftAdapter? = null
    private var rightAdapter: TypeRightAdapter? = null
    private var leftData: List<String>? = null
    private var rightData: List<String>? = null
    private var typeAreaModel: TypeAreaModel? = null
    private var typeModel: TypeModel? = null
    private var findModel: FindModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_type)

        initView()

    }

    private fun initView() {

        if (intent != null) {

            if (intent?.getStringExtra("from") != "") {
                from = intent?.getStringExtra("from")?:""
            }

            if (intent?.getStringExtra("ChooseType") != "" ) {
                type = intent?.getStringExtra("ChooseType")
                when (type) {
                    "area" -> {
                        val typeToken= object : TypeToken<TypeAreaModel>(){}.type
                        typeAreaModel = Gson().fromJson(getString(R.string.locationJson),typeToken)

                        var lIndex = 0
                        var rIndex = 0

                        if (!type1.isNullOrEmpty() || !intent.getStringExtra("data").isNullOrEmpty()) {
                            //storeedit
                            type1 = intent.getStringExtra("data")
                            //first choose
                            data = type1
                            lIndex = findIndex(typeAreaModel?.location!!,type1?:"",true)
                            rightData = filter(lIndex)
                            rIndex = findIndex(rightData!!,type1?:"",false)
                        }

                        leftData = typeAreaModel?.location!!

                        rvSetUp(lIndex, rIndex, type1?.substring(0,2)?:"", type1?:"")
                    }
                    "type" -> {

                        textView20.text = "選擇類別"
                        rv_fragment_type.visibility = View.GONE

                        var lIndex = 0
                        var rIndex = 0

                        if (from.contains("store")) {
                            val typeToken = object : TypeToken<TypeModel>() {}.type
                            typeModel =
                                Gson().fromJson(getString(R.string.typeJson), typeToken)

                            if (!type2.isNullOrEmpty() || !intent.getStringExtra("data").isNullOrEmpty()) {
                                type2 = intent.getStringExtra("data")
                                data = type2
                                rightData = filter(lIndex)
                                rIndex = findIndex(typeModel?.type!!,type2?:"",false)
                            }

                            rightData = typeModel?.type!!

                        } else {
                            val typeToken = object : TypeToken<FindModel>() {}.type
                            findModel =
                                Gson().fromJson(getString(R.string.findJson), typeToken)

                            if (!type2.isNullOrEmpty() || !intent.getStringExtra("data").isNullOrEmpty()) {
                                type2 = intent.getStringExtra("data")
                                data = type2
                                rightData = filter(lIndex)
                                rIndex = findIndex(findModel?.find!!,type2?:"",false)
                            }

                            rightData = findModel?.find
                        }

                        rvSetUp(0,rIndex,type2?:"", "")
                    }
                    "typeDetail" -> {

                        textView20.text = "選擇細項"
                        rv_fragment_type.visibility = View.GONE

                        if (from.contains("store")) {
                            val typeToken = object : TypeToken<TypeModel>() {}.type
                            typeModel = Gson().fromJson(getString(R.string.typeJson),typeToken)
                        } else {
                            val typeToken = object : TypeToken<FindModel>() {}.type
                            findModel =
                                Gson().fromJson(getString(R.string.findJson), typeToken)
                        }

                        var rIndex = 0

                        if (!type2.isNullOrEmpty() && !type3.isNullOrEmpty() || !intent.getStringExtra("data").isNullOrEmpty()) {
                            type2 = intent.getStringExtra("data")
                            type3 = intent.getStringExtra("data2")
                            data = type3
                            rightData = filter(null)
                            rIndex = findIndex(rightData!!, type3?:"", false)
                        }

                        rvSetUp(0,rIndex,type3?:"" ,"")
                    }
                }
            }
        }

        done_btn_type.setOnClickListener {
            when (type) {
                "area" -> type1 = data ?: "地點"
                "type" -> {
                    if (type2 != "" && type2 != "類別" && type2 != data) {
                        //換了種類,清除細項
                        type3 = "細項"
                    }

                    type2 = data ?: "類別"

                }
                "typeDetail" -> type3 = data ?: "細項"
            }

            finish()

        }

    }

    private fun rvSetUp(lIndex:Int,rIndex:Int,choose:String,choose2:String) {

        leftClick = { i,s ->
            rightData = filter(i)
            rightAdapter?.pos = 0
            rightAdapter?.choose = ""
//            data2 = s

            if (type == "area" ) {
                rightAdapter?.updateData(rightData!!)
            }

        }

        rightClick = {i,s ->

//            rightAdapter?.isSamePosition = rightAdapter?.pos == i
            rightAdapter?.pos = i

            if (rightAdapter?.choose != s) {
                var string = s
                if (s == "UIorUX") {
                    string = s.replace("or","/")
                }
                rightAdapter?.choose = string
            }

            rightAdapter?.notifyDataSetChanged()

            data = rightAdapter?.choose

        }

        if (type == "area") {
            var lIndex2: Int? = null
            var rIndex2: Int? = null
            lIndex2 = lIndex
            rIndex2 = rIndex
            if (lIndex == 0 && type1.isNullOrEmpty() ) {
                lIndex2 = null
            }
            if (rIndex == 0 && type1.isNullOrEmpty() ) {
                rIndex2 = null
            }
            leftAdapter = TypeLeftAdapter("type",leftData!!,this,lIndex2,choose,leftClick)
            rv_fragment_type.adapter = leftAdapter
            rightAdapter = TypeRightAdapter("type",true,rightData,this,rIndex2,choose2,rightClick)
        } else {
            rightAdapter = TypeRightAdapter("type",false,rightData,this,rIndex,choose,rightClick)
        }

        rv2_fragment_type.adapter = rightAdapter

    }

    private fun findIndex (dataList:List<String>,a:String,subString:Boolean):Int {
        var mIndex = 0
        var compare = a
        if (subString) {
            compare = a.substring(0,2)
        }

        for (i in dataList.indices) {
            if (dataList[i].contains(compare)) {
                mIndex = i
                if (compare == "新竹" || compare == "嘉義") {
                    var data = filter(mIndex)
                    var isContains = false
                    for (x in data.indices) {
                        if (data[x].contains(a)) {
                            isContains = true
                            break
                        }
                    }

                    if (!isContains) {
                        mIndex = i+1
                    }

                }
                return mIndex
            }
        }

        return mIndex
    }

    private fun filter(position:Int?):List<String> {
        var listData:List<String>? = null
        listData = listOf("")
        when (type) {
            "area" -> {
                when (position) {
                    0 -> listData = typeAreaModel?.Taipei
                    1 -> listData = typeAreaModel?.NewTaipei
                    2 -> listData = typeAreaModel?.Taoyuan
                    3 -> listData = typeAreaModel?.XinzhuCity
                    4 -> listData = typeAreaModel?.XinzhuCounty
                    5 -> listData = typeAreaModel?.Miaoli
                    6 -> listData = typeAreaModel?.Taizhong
                    7 -> listData = typeAreaModel?.Zhanghua
                    8 -> listData = typeAreaModel?.Nantou
                    9 -> listData = typeAreaModel?.Yunlin
                    10 -> listData = typeAreaModel?.ChiayiCity
                    11 -> listData = typeAreaModel?.ChiayiCounty
                    12 -> listData = typeAreaModel?.Tainan
                    13 -> listData = typeAreaModel?.Kaohsiung
                    14 -> listData = typeAreaModel?.Pingtung
                    15 -> listData = typeAreaModel?.Keelung
                    16 -> listData = typeAreaModel?.Yilan
                    17 -> listData = typeAreaModel?.Hualien
                    18 -> listData = typeAreaModel?.Taitung
                    19 -> listData = typeAreaModel?.Penghu
                    20 -> listData = typeAreaModel?.Kinmen
                    21 -> listData = typeAreaModel?.Lienchiang
                    22 -> listData = typeAreaModel?.all
                    23 -> listData = typeAreaModel?.remote
                }
                return listData?: listOf("")
            }
            "type" -> {
                if (from.contains("store")) {
                    when (position) {
                        0 -> listData = typeModel?.all
                        1 -> listData = typeModel?.art
                        2 -> listData = typeModel?.music
                        3 -> listData = typeModel?.media
                        4 -> listData = typeModel?.writer
                        5 -> listData = typeModel?.programing
                        6 -> listData = typeModel?.language
                        7 -> listData = typeModel?.consultation
                        8 -> listData = typeModel?.love
                        9 -> listData = typeModel?.beauty
                        10 -> listData = typeModel?.smart
                        11 -> listData = typeModel?.life
                        12 -> listData = typeModel?.show
                        13 -> listData = typeModel?.divine
                        14 -> listData = typeModel?.craft
                        15 -> listData = typeModel?.farmer
                        16 -> listData = typeModel?.service
                        17 -> {
                            listData = null
                            type3 = "其他"
                        }
                    }
                } else {
                    when (position) {
                        0 -> listData = findModel?.teach
                        1 -> listData = findModel?.service
                        2 -> listData = findModel?.group
                    }
                }
                return listData?: listOf("")
            }
            "typeDetail" -> {
                if(from.contains("store")) {
                    when (type2) {
                        "" -> listData= null
                        "全部類別" -> listData= typeModel?.all
                        "美術繪畫" -> listData= typeModel?.art
                        "音樂樂器" -> listData= typeModel?.music
                        "多媒體" -> listData= typeModel?.media
                        "文筆寫作" -> listData= typeModel?.writer
                        "程式設計" -> listData= typeModel?.programing
                        "語文" -> listData= typeModel?.language
                        "諮商" -> listData= typeModel?.consultation
                        "感情" -> listData= typeModel?.love
                        "美容時尚" -> listData= typeModel?.beauty
                        "益智" -> listData= typeModel?.smart
                        "生活" -> listData= typeModel?.life
                        "運動" -> listData= typeModel?.sport
                        "表演" -> listData= typeModel?.show
                        "命理" -> listData= typeModel?.divine
                        "工藝" -> listData= typeModel?.craft
                        "農藝" -> listData= typeModel?.farmer
                        "服務" -> listData= typeModel?.service
                        "其他類別" -> {
                            listData= null
                            type3 = "其他"
                        }
                    }
                } else {
                    when(type2) {
                        "" -> listData= null
                        "找教學" -> listData= findModel?.teach
                        "找服務" -> listData= findModel?.service
                        "揪團" -> listData= findModel?.group
                    }
                }
                return listData?: listOf("")
            }
        }
        return listData
    }
}