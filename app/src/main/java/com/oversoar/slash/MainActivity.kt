package com.oversoar.slash

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.appcompat.app.AlertDialog
import com.google.android.material.tabs.TabLayout
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Fragment.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()

    }

    private fun initView() {

        supportActionBar?.hide()

        val fm = supportFragmentManager.beginTransaction()
        fm.add(R.id.fragment_container,
            MainFragment()
        ).commit()

        tabLayout3.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
                tabPerform(tab?.position?:0)
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tabPerform(tab?.position?:0)
            }
        })
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        val f = supportFragmentManager.findFragmentById(R.id.fragment_container).toString()

        if (!f.contains("ChatFragment") && !f.contains("StoreFragment") && !f.contains("TypeAreaFragment")
                && !f.contains("EditInfoFragment") && !f.contains("EditPwdFragment")
                && !f.contains("FeedbackFragment") && !f.contains("MissionFragment") && !f.contains("GotoLoginFragment")) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                AlertDialog.Builder(this)
                    .setTitle("訊息提示")
                    .setMessage("是否離開APP？")
                    .setNegativeButton("確定") { dialog, which ->
                        finish()
                    }
                    .setPositiveButton("取消") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun tabPerform(position:Int) {
        when (position) {
            0 -> {
                supportActionBar?.hide()
                val fm = supportFragmentManager.beginTransaction()
                fm.replace(R.id.fragment_container,
                    MainFragment()
                ).commit()
            }
            1 -> {
                supportActionBar?.show()
                supportActionBar?.title = "焦點"
                val fm = supportFragmentManager.beginTransaction()
                fm.replace(R.id.fragment_container,
                    FocusFragment()
                ).commit()
            }
            2 -> {
                if (!userInfo?.user_id.isNullOrEmpty()) {
                    supportActionBar?.show()
                    supportActionBar?.title = "訊息"
                    val fm = supportFragmentManager.beginTransaction()
                    fm.replace(R.id.fragment_container,
                        MessageListFragment()
                    ).commit()
                } else {
                    supportActionBar?.hide()
                    val fm = supportFragmentManager.beginTransaction()
                    fm.replace(R.id.fragment_container,
                        GotoLoginFragment()
                    ).commit()
                }
            }
            3 -> {
                if (!userInfo?.user_id.isNullOrEmpty()) {
                    supportActionBar?.show()
                    supportActionBar?.title = "店鋪"
                    val fm = supportFragmentManager.beginTransaction()
                    fm.replace(
                        R.id.fragment_container,
                        StoreListFragment()
                    ).commit()
                } else {
                    supportActionBar?.hide()
                    val fm = supportFragmentManager.beginTransaction()
                    fm.replace(R.id.fragment_container,
                        GotoLoginFragment()
                    ).commit()
                }
            }
            4 -> {
                supportActionBar?.show()
                supportActionBar?.title = "設定"
                val fm = supportFragmentManager.beginTransaction()
                fm.replace(R.id.fragment_container,
                    SetFragment()
                ).commit()
            }
        }
    }
}
