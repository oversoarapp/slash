package com.oversoar.slash

import com.oversoar.slash.Model.MissionInfoModel
import com.oversoar.slash.Model.StoreInfoModel
import com.oversoar.slash.Model.UserInfoModel

object Commom {
    var userInfo: UserInfoModel? = null
    var userStoreData: List<StoreInfoModel>? = null
    var userMissionData: List<MissionInfoModel>? = null
    var userStoreFollow: List<StoreInfoModel>? = null
    var userMissionFollow: List<MissionInfoModel>? = null
    var fcmToken = ""
    //search/type
    var search:String? = null
    var type1:String? = null
    var type2:String? = null
    var type3:String? = null
    var typeList1: List<String>? = null
    var mainType2:String? = null
    var typeList3: List<String>? = null
}