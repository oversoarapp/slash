package com.oversoar.slash

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_signup.*
import java.io.ByteArrayOutputStream

class SignUpActivity: AppCompatActivity() {

    private val SELECT_PHOTO = 111
    private var imgUrl = ByteArray(0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        initView()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode){
            SELECT_PHOTO -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val stream = contentResolver?.openInputStream(data.data!!)
                    var bitmap = BitmapFactory.decodeStream(stream)
                    val baos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    imgUrl = baos.toByteArray()

                    Picasso.get().load(data.data!!).into(imageView9)

                }
            }
        }
    }

    private fun initView() {

        supportActionBar?.title = "註冊"

        imageView9.setOnClickListener {
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            startActivityForResult(intent, SELECT_PHOTO)
        }

        done_btn_sigup.setOnClickListener {
            if (account_signup.text.toString().length == 10) {
                if (pwd_signup.text.toString().length >= 6) {
                    if (pwd_signup.text.toString() == repwd_signup.text.toString()) {
                        val intent = Intent(this, CAPTCHAActivity::class.java)
                        intent.putExtra(CAPTCHAActivity.USERACC, account_signup.text.toString())
                        intent.putExtra(CAPTCHAActivity.USERPWD, pwd_signup.text.toString())
                        intent.putExtra(CAPTCHAActivity.SIGNUPTYPE, "0")
                        intent.putExtra(CAPTCHAActivity.USERNAME, name_signup.text.toString())
                        intent.putExtra(CAPTCHAActivity.USERIMGURL, imgUrl)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this, "密碼與確認密碼不符！", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, "密碼長度需六碼以上！", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "手機號碼應為十碼！", Toast.LENGTH_SHORT).show()
            }
        }
    }
}