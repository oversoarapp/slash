package com.oversoar.slash.Model

data class StoreModel(
    val msg: String,
    val result: List<StoreInfoModel>,
    val success: Boolean
)