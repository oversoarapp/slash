package com.oversoar.slash.Model

data class FindModel(
    val find: List<String>,
    val group: List<String>,
    val service: List<String>,
    val teach: List<String>
)