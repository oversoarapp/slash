package com.oversoar.slash.Model

data class TypeAreaModel(
    val location:List<String>,
    val ChiayiCity: List<String>,
    val ChiayiCounty: List<String>,
    val Kaohsiung: List<String>,
    val Kinmen: List<String>,
    val Lienchiang: List<String>,
    val Miaoli: List<String>,
    val Nantou: List<String>,
    val Penghu: List<String>,
    val Pingtung: List<String>,
    val Tainan: List<String>,
    val Taitung: List<String>,
    val Taizhong: List<String>,
    val Taoyuan: List<String>,
    val XinzhuCity: List<String>,
    val XinzhuCounty: List<String>,
    val Yunlin: List<String>,
    val Zhanghua: List<String>,
    val Hualien: List<String>,
    val NewTaipei: List<String>,
    val Taipei: List<String>,
    val Keelung: List<String>,
    val Yilan: List<String>,
    val all: List<String>,
    val remote: List<String>
)