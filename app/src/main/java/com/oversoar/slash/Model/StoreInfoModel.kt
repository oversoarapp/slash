package com.oversoar.slash.Model

data class StoreInfoModel(
    val created_date: String? = null,
    val id: String? = null,
    val image_URL_1: String ? = null,
    val image_URL_2: String? = null,
    val image_URL_3: String? = null,
    val image_URL_4: String? = null,
    val location: String? = null,
    val states: String? = null,
    val first: String? = null,
    val store_detail: String? = null,
    val store_name: String? = null,
    val type: String? = null,
    val type_detail: String? = null,
    val user_id: String? = null,
    val expiry_date:String? = null
)