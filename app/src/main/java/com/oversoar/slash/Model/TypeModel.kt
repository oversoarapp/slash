package com.oversoar.slash.Model

data class TypeModel(
    val all: List<String>,
    val art: List<String>,
    val beauty: List<String>,
    val consultation: List<String>,
    val craft: List<String>,
    val divine: List<String>,
    val farmer: List<String>,
    val language: List<String>,
    val life: List<String>,
    val love: List<String>,
    val media: List<String>,
    val music: List<String>,
    val programing: List<String>,
    val service: List<String>,
    val show: List<String>,
    val smart: List<String>,
    val sport: List<String>,
    val type: List<String>,
    val writer: List<String>
)