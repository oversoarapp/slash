package com.oversoar.slash.Model

data class UserDBModel(
    val user_id:String,
    val user_name:String,
    val photoURL:String,
    val block:ArrayList<String>
) {
    constructor(): this("","","", arrayListOf())
}