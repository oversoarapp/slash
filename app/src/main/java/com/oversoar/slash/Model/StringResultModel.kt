package com.oversoar.slash.Model

data class StringResultModel(
    val msg: String,
    val result: String,
    val success: Boolean
)