package com.oversoar.slash.Model

data class SingleStoreModel(
    val msg: String,
    val result: StoreInfoModel,
    val success: Boolean
)