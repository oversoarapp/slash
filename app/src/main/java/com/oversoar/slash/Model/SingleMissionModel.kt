package com.oversoar.slash.Model

data class SingleMissionModel(
    val msg: String,
    val result: MissionInfoModel,
    val success: Boolean
)