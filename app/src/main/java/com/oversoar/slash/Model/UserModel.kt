package com.oversoar.slash.Model

data class UserModel(
    val msg: String,
    val result: UserInfoModel,
    val success: Boolean,
    val store_follows: List<StoreInfoModel>,
    val mission_follows: List<MissionInfoModel>
)