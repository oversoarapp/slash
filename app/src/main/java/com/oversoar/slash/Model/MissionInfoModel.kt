package com.oversoar.slash.Model

data class MissionInfoModel(
    val created_date: String? = null,
    val expiry_date: String? = null,
    val find: String? = null,
    val find_detail: String? = null,
    val id: String? = null,
    val image_URL_1: String? = null,
    val image_URL_2: String? = null,
    val image_URL_3: String? = null,
    val image_URL_4: String? = null,
    val location: String? = null,
    val mission_detail: String? = null,
    val mission_name: String? = null,
    val states: String? = null,
    val user_id: String? = null
)