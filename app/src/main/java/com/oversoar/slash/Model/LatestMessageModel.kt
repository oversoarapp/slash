package com.oversoar.slash.Model

data class LatestMessageModel(
        val count:Int,
        val date:String,
        val text:String,
        val from_id:String
) {
    constructor(): this(0,"","","")
}