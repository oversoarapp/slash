package com.oversoar.slash.Model

data class UserInfoModel(
    val FCM_token: String? = null,
    val created_at: String? = null,
    val facebook_email: String? = null,
    val google_email: String? = null,
    val user_id: String? = null,
    var image_URL: String? = null,
    val latest_login_at: String? = null,
    var user_name: String? = null,
    var password: String? = null,
    val phone: String? = null
)