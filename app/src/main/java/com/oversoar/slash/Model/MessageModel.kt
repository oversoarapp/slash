package com.oversoar.slash.Model

data class MessageModel(
        val date:String,
        val text:String,
        val from_id:String
) {
    constructor(): this("","","")
}