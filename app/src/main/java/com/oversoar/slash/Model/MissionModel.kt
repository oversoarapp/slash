package com.oversoar.slash.Model

data class MissionModel(
    val msg: String? = null,
    val result: List<MissionInfoModel>? = null,
    val success: Boolean? = null
)