package com.oversoar.slash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.facebook.*
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Commom.fcmToken
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Model.UserModel
import com.oversoar.slash.Remote.LoginApi
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class LoginActivity : AppCompatActivity() {

    private val GOOGLE_LOGIN_REQUESTCODE = 111
    private val FB_LOGIN_REQUESTCODE = 64206
    private var callbackManager: CallbackManager? = null
    private var name = ""
    private var imgUrl = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val intent = Intent(this,MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            startActivity(intent)
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            GOOGLE_LOGIN_REQUESTCODE -> {
                googleLoginResult(data)
            }

            FB_LOGIN_REQUESTCODE -> {
                callbackManager?.onActivityResult(requestCode, resultCode, data)
                Log.d("fblogin", data.toString())
            }
        }
    }

    private fun initView() {

        supportActionBar?.hide()

        google_btn_login.setOnClickListener {
            googleLogin()
        }

        fb_login_btn_login.setOnClickListener {
            fbLogin()
        }

        done_btn_login.setOnClickListener {
            val acc = account_login.text.toString()
            val pwd = pwd_login.text.toString()
            login(acc,pwd,"0")
        }

        signup_btn_login.setOnClickListener {
            val intent = Intent(this,SignUpActivity::class.java)
            startActivity(intent)
        }

        forget_pwd_login.setOnClickListener {
            val intent = Intent(this, ForgetPwdActivity::class.java)
            startActivity(intent)
        }

    }

    private fun login(acc:String,pwd:String,type:String) {

        CoroutineScope(Dispatchers.Main).launch {

            var state: ApiResult<UserModel>

            withContext(Dispatchers.IO) {
                state = LoginApi()
                    .login(acc, pwd, type, fcmToken)
            }

            when (state) {

                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success) {
                        //有response
                        userInfo = (state as ApiResult.Success).data.result
                        Commom.userStoreFollow = (state as ApiResult.Success).data.store_follows
                        Commom.userMissionFollow = (state as ApiResult.Success).data.mission_follows

                        val sp = getSharedPreferences("userData", Context.MODE_PRIVATE)
                        val edit = sp.edit()
                        edit.putString("acc",acc)
                        edit.putString("pwd",pwd).apply()

                        val intent = Intent(this@LoginActivity,MainActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        Log.i("LoginActivity","saveUserToDB Successfully")

                        Log.i("SUCCESS", (state as ApiResult.Success).data.result.toString())
                    } else {
                        //登入失敗 / 申請帳號
                        when (type) {
                            "0" -> {
                                Toast.makeText(
                                    this@LoginActivity,
                                    (state as ApiResult.Success).data.msg,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            "1","2" -> {
                                val intent = Intent(this@LoginActivity, CAPTCHAActivity::class.java)
                                intent.putExtra(CAPTCHAActivity.SIGNUPTYPE,type)
                                intent.putExtra(CAPTCHAActivity.USERNAME,name)
                                intent.putExtra(CAPTCHAActivity.USERACC,acc)
                                intent.putExtra(CAPTCHAActivity.USEREMAIL,acc)
                                intent.putExtra(CAPTCHAActivity.USERIMGURL,imgUrl)
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK))
                                startActivity(intent)
                            }
                        }
                        Log.i("Failure", (state as ApiResult.Success).data.msg)
                    }
                }
                is ApiResult.Error -> {
                    AlertDialog.Builder(this@LoginActivity)
                        .setTitle("訊息提示")
                        .setMessage("網路或系統有誤，請稍後再試！")
                        .show()
                    Log.e("ERROR", Log.getStackTraceString((state as ApiResult.Error).exception))
                }
            }
        }
    }

    private fun googleLogin() {
//        val id =  getString(R.string.server_client_id)
        var mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            //            .requestIdToken(id)
            .requestEmail()
            .build()
        val mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, GOOGLE_LOGIN_REQUESTCODE)
    }

    private fun googleLoginResult(data: Intent?) {
        val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)

        if (result.isSuccess) {
            try {
                val account = result.signInAccount
                name = account?.displayName?:""
                imgUrl = account?.photoUrl?.toString()?:""
                login(account?.email?:"","","2")
                Log.i("googleProfile","name:${account?.displayName},id:${account?.id},email:${account?.email}")
                Log.i("googleUserImg","imgUrl:$imgUrl")
            } catch (e: ApiException) {
                Log.d("googleLogin-error:", e.toString())
            }
        } else {
            Log.d("googleLogin-failed:", result.toString())
        }
    }

    private fun fbLogin() {
        val loginManager = LoginManager.getInstance()
        callbackManager = CallbackManager.Factory.create()
        loginManager.loginBehavior = LoginBehavior.NATIVE_WITH_FALLBACK
        val permission = mutableSetOf<String>("email", "public_profile")
        loginManager.logInWithReadPermissions(this, permission)
//        loginManager.logInWithPublishPermissions(this, mutableListOf("publish_actions"))
        loginManager.registerCallback(
            callbackManager, object : FacebookCallback<com.facebook.login.LoginResult> {
                override fun onSuccess(loginResult: com.facebook.login.LoginResult) {

                    val request =
                        GraphRequest.newMeRequest(loginResult.accessToken) { result, response ->
                            try {
                                //here is the data that you want
                                Log.d("FBLOGIN_JSON_RES", result.toString())

                                if (result.has("name")) {
                                    name = result.getString("name")
                                    val picture = result.getJSONObject("picture").getJSONObject("data")
                                    imgUrl = picture.getString("url")
                                    val email = result.getString("email")
                                    login(email,"","1")
                                    Log.i("FBPROFILE", result.toString())
                                } else {
                                    Log.e("FBLOGIN_FAILD", result.toString())
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                    val parameters = Bundle()
                    parameters.putString("fields", "name,email,id,picture.type(large)")
                    request.parameters = parameters
                    request.executeAsync()

                }

                override fun onCancel() {
                }

                override fun onError(error: FacebookException?) {
                    Log.d("FBLOGIN-ERROR", error.toString())
                    Toast.makeText(
                        this@LoginActivity,
                        error.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }
}