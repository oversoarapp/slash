package com.oversoar.slash.Fragment

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.google.gson.Gson
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Adapter.UserMissionListAdapter
import com.oversoar.slash.Adapter.UserStoreListAdapter
import com.oversoar.slash.Commom
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Commom.userMissionData
import com.oversoar.slash.Commom.userStoreData
import com.oversoar.slash.Model.MissionModel
import com.oversoar.slash.Model.StoreModel
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import kotlinx.android.synthetic.main.fragment_store_list.*
import kotlinx.coroutines.*

class StoreListFragment : Fragment() {

    private val TAG = "StoreListFragment"
    private var nAdapter: UserStoreListAdapter? = null
    private var mAdapter: UserMissionListAdapter? = null
    private var job: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_store_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        val click:(Int)->Unit = {

            Commom.type1 = null
            Commom.type2 = null
            Commom.type3 = null

            val bundle = Bundle()
            bundle.putString("storeData",Gson().toJson(userStoreData!![it]))
            bundle.putString("from","user")
            val fm = StoreFragment()
            fm.arguments = bundle

            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.addToBackStack(null)
            ft?.replace(R.id.fragment_container, fm)?.commit()
        }

        val missionClick:(Int)->Unit = {

            Commom.type1 = null
            Commom.type2 = null
            Commom.type3 = null

            val fm = MissionFragment()
            val bundle = Bundle()
            bundle.putString("from","user")
            bundle.putString("missionData", Gson().toJson(userMissionData!![it]))
            fm.arguments = bundle

            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.fragment_container, fm)
            ft?.addToBackStack(null)
            ft?.commit()
        }

        val payClick:(String,Int)->Unit = { s,i ->
            val fm = WebViewFragment()
            val bundle = Bundle()
            if (s == "store") {
                bundle.putString("data", Gson().toJson(userStoreData!![i]))
                bundle.putString("item", "1")
                fm.arguments = bundle
            } else {
                bundle.putString("data", Gson().toJson(userMissionData!![i]))
                bundle.putString("item", "2")
                fm.arguments = bundle
            }
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.fragment_container, fm)
            ft?.addToBackStack(null)
            ft?.commit()
        }

        val deleteClick:(String,Int)->Unit = {s,i ->
            AlertDialog.Builder(context)
                    .setTitle("訊息提示")
                    .setMessage("是否確定要刪除？")
                    .setNegativeButton("確定") {d,w ->
                        if (s == "store") {
                            deleteStore(userStoreData!![i].id?:"")
                        } else {
                            deleteMission(userMissionData!![i].id?:"")
                        }
                    }
                    .setPositiveButton("取消") {d,w ->
                        d.cancel()
                    }
                    .show()
        }

        new_mission_store_list.setOnClickListener {

            if (userMissionData?.size?:0 >= 1 ) {
                return@setOnClickListener
            }

            Commom.type1 = null
            Commom.type2 = null
            Commom.type3 = null

            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.addToBackStack(null)
            ft?.replace(R.id.fragment_container, NewMissionFragment())?.commit()
        }

        new_store_store_list.setOnClickListener {

            when (userStoreData?.size?:0) {
                0 -> {
                    AlertDialog.Builder(context)
                            .setTitle("新增店舖")
                            .setMessage("第一間店舖為永久免費,是否確認增加新店舖？")
                            .setNegativeButton("確認") {d,w ->
                                val ft = activity?.supportFragmentManager?.beginTransaction()
                                ft?.addToBackStack(null)
                                ft?.replace(R.id.fragment_container, NewStoreFragment())?.commit()
                            }
                            .setPositiveButton("取消") {d,w ->
                                d.cancel()
                            }
                            .show()
                }
                1,2 -> {
                    AlertDialog.Builder(context)
                            .setTitle("新增店舖")
                            .setMessage("每個月酌收系統維護費用50元,是否確認增加新店舖？")
                            .setNegativeButton("確認") {d,w ->
                                val ft = activity?.supportFragmentManager?.beginTransaction()
                                ft?.addToBackStack(null)
                                ft?.replace(R.id.fragment_container, NewStoreFragment())?.commit()
                            }
                            .setPositiveButton("取消") {d,w ->
                                d.cancel()
                            }
                            .show()
                }
                3 -> return@setOnClickListener
                else -> return@setOnClickListener
            }

            Commom.type1 = null
            Commom.type2 = null
            Commom.type3 = null

        }

        if (userStoreData?.size ?: 0 >= 3) {
            new_store_store_list.background = context?.getDrawable(R.drawable.rounded_gray_shape)
        } else {
            new_store_store_list.background = context?.getDrawable(R.drawable.rounded_green_shape)
        }

        if (userStoreData?.size ?: 0 >= 1 ) {
            new_mission_store_list.background = context?.getDrawable(R.drawable.rounded_gray_shape)
        } else {
            new_mission_store_list.background = context?.getDrawable(R.drawable.rounded_green_shape)
        }

        nAdapter = UserStoreListAdapter(context!!,userStoreData,click,payClick,deleteClick)
        mAdapter = UserMissionListAdapter(context!!,userMissionData,missionClick,payClick,deleteClick)
        rv_store_fragment.adapter = nAdapter
        rv2_store_fragment.adapter = mAdapter
        rv_store_fragment.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        rv2_store_fragment.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

    }

    private fun getData() {
        job = CoroutineScope(Dispatchers.Main).launch {
            var storeResult: ApiResult<StoreModel>
            var missionResult: ApiResult<MissionModel>

            withContext(Dispatchers.IO) {
                storeResult = getStoreData(userInfo?.user_id?:"")
                missionResult = getMissionData(userInfo?.user_id?:"")
            }

            when (storeResult) {
                is ApiResult.Success -> {
                    if ((storeResult as ApiResult.Success<StoreModel>).data.success) {
                        if (!(storeResult as ApiResult.Success<StoreModel>).data.result.isNullOrEmpty()) {
//                            textView7.visibility = View.GONE
                            userStoreData = (storeResult as ApiResult.Success<StoreModel>).data.result
                            nAdapter?.updateData((storeResult as ApiResult.Success<StoreModel>).data.result)

                            if (userStoreData?.size ?: 0 >= 3) {
                                new_store_store_list.background = context?.getDrawable(R.drawable.rounded_gray_shape)
                            } else {
                                new_store_store_list.background = context?.getDrawable(R.drawable.rounded_green_shape)
                            }

                        } else {
//                            textView7.visibility = View.VISIBLE
//                            textView7.text = "您尚未建立店舖"
                            new_store_store_list.background = context?.getDrawable(R.drawable.rounded_green_shape)
                            userStoreData = null
                            nAdapter?.updateData(userStoreData)
                        }
//                    } else {
//                        textView7.visibility = View.VISIBLE
//                        textView7.text = "您尚未建立店舖"
                    } else {
                        new_store_store_list.background = context?.getDrawable(R.drawable.rounded_green_shape)
                        userStoreData = null
                        nAdapter?.updateData(userStoreData)
                    }


                    Log.i(TAG, (storeResult as ApiResult.Success<StoreModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Log.i(TAG, (storeResult as ApiResult.Error).exception.toString())
                }
            }

            when (missionResult) {
                is ApiResult.Success -> {
                    if ((missionResult as ApiResult.Success<MissionModel>).data.success == true ) {
                        if (!(missionResult as ApiResult.Success<MissionModel>).data.result.isNullOrEmpty()) {
//                            textView7.visibility = View.GONE
                            userMissionData = (missionResult as ApiResult.Success<MissionModel>).data.result!!
                            mAdapter?.updateData((missionResult as ApiResult.Success<MissionModel>).data.result)

                            if (userMissionData?.size ?: 0 >= 1 ) {
                                new_mission_store_list.background = context?.getDrawable(R.drawable.rounded_gray_shape)
                            } else {
                                new_mission_store_list.background = context?.getDrawable(R.drawable.rounded_green_shape)
                            }

                        } else {
//                            textView7.visibility = View.VISIBLE
                            userMissionData = null
                            mAdapter?.updateData(userMissionData)
                        }
//                    } else {
//                        textView7.visibility = View.VISIBLE
//                        textView7.text = "您尚未建立即時任務"
                    }
                    Log.i(TAG, (missionResult as ApiResult.Success<MissionModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Log.i(TAG, (missionResult as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private fun deleteStore(storeId: String) {

        progressBar8?.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {

            var result: ApiResult<StoreModel>

            withContext(Dispatchers.IO) {
                result = deleteStoreApi(storeId)
            }

            progressBar8?.visibility = View.GONE

            when (result) {
                is ApiResult.Success -> {
                    if ((result as ApiResult.Success<StoreModel>).data.success) {
                        if (!(result as ApiResult.Success<StoreModel>).data.result.isNullOrEmpty()) {

                            deleteFirebaseDb("store",storeId)
                            userStoreData = (result as ApiResult.Success<StoreModel>).data.result
                            nAdapter?.updateData((result as ApiResult.Success<StoreModel>).data.result)

                            if (userStoreData?.size ?: 0 >= 3) {
                                new_store_store_list.background = context?.getDrawable(R.drawable.rounded_gray_shape)
                            } else {
                                new_store_store_list.background = context?.getDrawable(R.drawable.rounded_green_shape)
                            }

                        } else {
                            userStoreData = null
                            nAdapter?.updateData(userStoreData)
                        }
                    }
                    Toast.makeText(context,"刪除成功！",Toast.LENGTH_SHORT).show()
                    Log.i(TAG, (result as ApiResult.Success<StoreModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Toast.makeText(context,"刪除失敗！",Toast.LENGTH_SHORT).show()
                    Log.i(TAG, (result as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private fun deleteMission(missionId: String) {

        progressBar8?.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {

            var result: ApiResult<MissionModel>

            withContext(Dispatchers.IO) {
                result = deleteMissionApi(missionId)
            }

            progressBar8?.visibility = View.GONE

            when (result) {
                is ApiResult.Success -> {
                    if ((result as ApiResult.Success<MissionModel>).data.success == true ) {
                        if (!(result as ApiResult.Success<MissionModel>).data.result.isNullOrEmpty()) {

                            deleteFirebaseDb("mission",missionId)
                            userMissionData = (result as ApiResult.Success<MissionModel>).data.result!!
                            mAdapter?.updateData((result as ApiResult.Success<MissionModel>).data.result)

                        } else {
                            userMissionData = null
                            mAdapter?.updateData(userMissionData)
                        }

                        if (userMissionData?.size ?: 0 >= 1) {
                            new_mission_store_list.background = context?.getDrawable(R.drawable.rounded_gray_shape)
                        } else {
                            new_mission_store_list.background = context?.getDrawable(R.drawable.rounded_green_shape)
                        }

                    }
                    Toast.makeText(context,"刪除成功！",Toast.LENGTH_SHORT).show()
                    Log.i(TAG, (result as ApiResult.Success<MissionModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Toast.makeText(context,"刪除失敗！",Toast.LENGTH_SHORT).show()
                    Log.i(TAG, (result as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private fun deleteFirebaseDb(delete:String,id:String) {
        val storage = Firebase.storage
        var ref: StorageReference? = null
        when (delete) {
            "store" -> {
                for (i in 0..3) {
                    ref = storage.reference.child("user${userInfo?.user_id ?: ""}/store$id/detail$i.jpg")
                    ref.delete()
                }
            }
            "mission" -> {
                for (i in 0..3) {
                    ref = storage.reference.child("user${userInfo?.user_id ?: ""}/mission$id/detail$i.jpg")
                    ref.delete()
                }
            }
        }
    }

    private suspend fun deleteStoreApi(storeId:String): ApiResult<StoreModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).deleteStore(userInfo?.user_id?:"",storeId)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun deleteMissionApi(missionId:String): ApiResult<MissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).deleteMission(userInfo?.user_id?:"",missionId)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getStoreData(id:String): ApiResult<StoreModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getUserStore(id)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getMissionData(id:String): ApiResult<MissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getUserMission(id)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    override fun onStop() {
        super.onStop()
        job?.cancel()
    }
}
