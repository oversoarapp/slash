package com.oversoar.slash.Fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Adapter.GridViewAdapter2
import com.oversoar.slash.Commom
import com.oversoar.slash.Commom.type1
import com.oversoar.slash.Commom.type2
import com.oversoar.slash.Commom.type3
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Model.*
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import com.oversoar.slash.TypeActivity
import kotlinx.android.synthetic.main.fragment_editinfo.*
import kotlinx.android.synthetic.main.fragment_store.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import kotlin.math.max
import kotlin.math.sqrt

class NewMissionFragment: Fragment() {

    private val TAG = "NewMissionFragment"
    private val SELECT_PHOTO = 111
    private var storage = Firebase.storage
    private val imgByteList = mutableListOf<ByteArray>(ByteArray(0),ByteArray(0),ByteArray(0),ByteArray(0))
    private val imgList = mutableListOf<String>("","","","")
    private var nAdapter: GridViewAdapter2? = null
    private var position: Int? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_store,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    override fun onResume() {
        super.onResume()

        initView()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode){
            SELECT_PHOTO -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val stream = context?.contentResolver?.openInputStream(data.data!!)
                    var bitmap = BitmapFactory.decodeStream(stream)
                    val baos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    val byteArray = baos.toByteArray()
                    imgByteList[position?:0] = byteArray
                    imgList[position?:0] = data.data.toString()
                    nAdapter?.updateData(imgList)
                }
            }
        }
    }

    private fun initView() {

        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.title = "新建限時任務"

        linearLayout3.visibility = View.GONE
        new_btn_store.visibility = View.VISIBLE

        et_name_fragment_store.isEnabled = true
        et_content_ragment_store.isEnabled = true
        et_name_fragment_store.hint = "請輸入任務名稱"
        et_content_ragment_store.hint = "請輸入任務介紹"
        area_btn_store.text = type1?:"地點"
        skills_btn_store.text = type2?:"種類"
        type_btn_store.text = type3?:"細項"

        area_btn_store.setOnClickListener {

            val intent = Intent(context, TypeActivity::class.java)
            intent.putExtra("ChooseType","area")
            intent.putExtra("from","mission")

            startActivity(intent)

        }

        skills_btn_store.setOnClickListener {

            val intent = Intent(context, TypeActivity::class.java)
            intent.putExtra("ChooseType","type")
            intent.putExtra("from","mission")

            startActivity(intent)

        }

        type_btn_store.setOnClickListener {

            val intent = Intent(context, TypeActivity::class.java)
            intent.putExtra("ChooseType","typeDetail")
            intent.putExtra("from","mission")

            startActivity(intent)

        }

        val updatePhoto:(Int)-> Unit = {
            position = it
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            startActivityForResult(intent, SELECT_PHOTO)
        }

        val deleteClick:(Int)-> Unit = {
            imgByteList[0] = ByteArray(0)
            imgList[0] = ""
            nAdapter?.updateData(imgList)
        }

        nAdapter = GridViewAdapter2(context!!,imgList,updatePhoto,deleteClick)
        gv_fragment_store.adapter = nAdapter
        et_content_ragment_store.movementMethod = ScrollingMovementMethod.getInstance()

        new_btn_store.setOnClickListener {
            if (type1.isNullOrEmpty() || type2.isNullOrEmpty() || type3.isNullOrEmpty()) {
                Toast.makeText(context,"地區或種類不得為空！",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val name = et_name_fragment_store.text.toString()
            val content = et_content_ragment_store.text.toString()

            if (name.isEmpty() || content.isEmpty()) {
                Toast.makeText(context,"名稱或介紹不得為空！",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            newMission(name,content, type1?:"", type2?:"", type3?:"")
        }

    }

    private fun newMission(name:String, content:String, location:String, type:String, typeDetail:String) {

        progressBar3.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {
            var result: ApiResult<SingleMissionModel>
            var missionDataResult: ApiResult<MissionModel>
            withContext(Dispatchers.IO) {
                result = newMissionResponse(name, content, location, type, typeDetail)
                missionDataResult = getMissionData(userInfo?.user_id?:"")

            }

            when (missionDataResult) {
                is ApiResult.Success -> {
                    Commom.userMissionData = (missionDataResult as ApiResult.Success<MissionModel>).data.result
                    Log.i(TAG, (missionDataResult as ApiResult.Success<MissionModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Log.i(TAG, (missionDataResult as ApiResult.Error).exception.toString())
                }
            }


            when (result) {
                is ApiResult.Success -> {
                    val id = (result as ApiResult.Success<SingleMissionModel>).data.result.id?:""

                    if (imgList.isNotEmpty()) {

                        uploadImg(id)

                        progressBar3.visibility = View.GONE
                        Toast.makeText(context, "創建成功！", Toast.LENGTH_SHORT).show()
                        val fm = activity?.supportFragmentManager?.beginTransaction()
                        fm?.replace(R.id.fragment_container, StoreListFragment())?.commit()
                    } else {
                        progressBar3.visibility = View.GONE
                        Toast.makeText(context, "創建成功！", Toast.LENGTH_SHORT).show()
                        val fm = activity?.supportFragmentManager?.beginTransaction()
                        fm?.replace(R.id.fragment_container, StoreListFragment())?.commit()
                    }

                    Log.i(TAG, (result as ApiResult.Success<SingleMissionModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Log.i(TAG, (result as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private fun uploadImg (missionId:String) {
        CoroutineScope(Dispatchers.IO).launch {
            imgByteList.forEachIndexed { index, bytes ->
                if (bytes.isNotEmpty()) {
                    val ref =
                        storage?.reference?.child("user${userInfo?.user_id?:""}/mission$missionId/detail${index + 1}.jpg")

                    val uploadTask = ref?.putBytes(bytes)

                    uploadTask?.continueWithTask { task ->
                        if (!task.isSuccessful) {
                            task.exception?.let {
                                throw it
                            }
                        }
                        ref.downloadUrl
                    }?.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            val downloadUri = task.result.toString()
                            CoroutineScope(Dispatchers.IO).launch {
                                ApiClient().createService(ApiService::class.java)
                                    .uploadMissionImg(
                                        missionId,
                                        userInfo?.user_id ?: "",
                                        index + 1,
                                        downloadUri
                                    )
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun newMissionResponse(name:String, content:String, location:String, find:String, findDetail:String): ApiResult<SingleMissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).saveMission("0",name,content,location,find,findDetail,
                    userInfo?.user_id?:"")
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getMissionData(id:String): ApiResult<MissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getUserMission(id)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        type1 = null
        type2 = null
        type3 = null

    }
}