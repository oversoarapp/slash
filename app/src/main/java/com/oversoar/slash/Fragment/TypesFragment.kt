package com.oversoar.slash.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.oversoar.slash.Adapter.TypeLeftAdapter
import com.oversoar.slash.Adapter.TypeRightAdapter
import com.oversoar.slash.Commom.type1
import com.oversoar.slash.Commom.mainType2
import com.oversoar.slash.Commom.type3
import com.oversoar.slash.Commom.typeList1
import com.oversoar.slash.Commom.typeList3
import com.oversoar.slash.Model.FindModel
import com.oversoar.slash.Model.TypeAreaModel
import com.oversoar.slash.Model.TypeModel
import com.oversoar.slash.R
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_type.*

class TypesFragment: Fragment() {

    private var type: String? = null
    //0 == 店舖,1 == 限時任務
    private var from = 0
    private var data = mutableListOf<String>()
    private var data2 = ""
    private var left = ""
    private lateinit var leftClick:(Int,String)->Unit
    private lateinit var rightClick:(Int,String)->Unit
    private var leftAdapter: TypeLeftAdapter? = null
    private var rightAdapter: TypeRightAdapter? = null
    private var leftData: List<String>? = null
    private var rightData: List<String>? = null
    private var typeAreaModel: TypeAreaModel? = null
    private var typeModel: TypeModel? = null
    private var findModel: FindModel? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_type,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setData()
        initView()

    }

    private fun initView() {

        clear_btn_type.visibility = View.VISIBLE

        clear_btn_type.setOnClickListener {
            when (type) {
                "area" -> data = mutableListOf<String>()
                "type" -> data2 = ""
                "typeDetail" -> data = mutableListOf<String>()
            }

            rightAdapter?.choose = ""
            rightAdapter?.notifyDataSetChanged()

        }

        done_btn_type.setOnClickListener {
            when (type) {
                "area" -> {
                    typeList1 = if (data.size >= 1) {
                        if (data.contains("")) {
                            data.remove("")
                        }
                        data
                    } else {
                        null
                    }
                }
                "type" -> {

                    if (mainType2 != "" && mainType2 != "類別" && mainType2 != data2) {
                        //換了種類,清除細項
                        typeList3 = null
                    }

                    mainType2 = data2

                    mainType2 = if (mainType2?.isEmpty() == true) {
                        null
                    } else {
                        data2
                    }

                }
                "typeDetail" -> {
                    typeList3 = if (data.size >= 1) {
                        if (data.contains("")) {
                            data.remove("")
                        }
                        data
                    } else {
                        null
                    }
                }
            }

            val fg = MainFragment()
            val bundle = Bundle()
            bundle.putInt("from",from)
            fg.arguments = bundle
            val fm = activity?.supportFragmentManager?.beginTransaction()
            fm?.replace(R.id.fragment_container, fg)?.commit()
        }

    }

    private fun setData() {
        if (arguments != null) {

            if (arguments?.getInt("from") != null) {
                from = arguments?.getInt("from")!!
            }

            if (arguments?.getString("ChooseType","") != "") {
                type = arguments?.getString("ChooseType","")
                when (type) {
                    "area" -> {

                        divider5.visibility = View.VISIBLE

                        val typeToken= object : TypeToken<TypeAreaModel>(){}.type
                        typeAreaModel = Gson().fromJson(context?.getString(R.string.locationJson),typeToken)

                        if (!typeList1.isNullOrEmpty()) {
                            typeList1?.forEach {
                                data.add(it)
                            }
                        }

                        leftData = typeAreaModel?.location!!

                        rvSetUp( (typeList1?: listOf("")).toString().replace("[","").replace("]","").replace(" ",""))
                    }
                    "type" -> {

                        textView20.text = "選擇類別"
                        rv_fragment_type.visibility = View.GONE

                        if (from == 0) {
                            val typeToken = object : TypeToken<TypeModel>() {}.type
                            typeModel =
                                    Gson().fromJson(context?.getString(R.string.typeJson), typeToken)
                            rightData = typeModel?.type
                        } else {
                            val typeToken = object : TypeToken<FindModel>() {}.type
                            findModel =
                                    Gson().fromJson(context?.getString(R.string.findJson), typeToken)
                            rightData = findModel?.find
                        }

                        if (!mainType2.isNullOrEmpty() && mainType2 != "類別") {
                            data2 = mainType2 ?: ""
                        }

                        rvSetUp(mainType2?:"")

                    }
                    "typeDetail" -> {

                        textView20.text = "選擇細項"
                        rv_fragment_type.visibility = View.GONE

                        if (from == 0) {
                            val typeToken = object : TypeToken<TypeModel>() {}.type
                            typeModel =
                                    Gson().fromJson(context?.getString(R.string.typeJson), typeToken)
                            rightData = typeModel?.type
                        } else {
                            val typeToken = object : TypeToken<FindModel>() {}.type
                            findModel =
                                    Gson().fromJson(context?.getString(R.string.findJson), typeToken)
                            rightData = findModel?.find
                        }

                        filter(null)

                        if (!typeList3.isNullOrEmpty()) {
                            typeList3?.forEach {
                                data.add(it)
                            }
                        }

                        rvSetUp( typeList3.toString().replace("[","").replace("]","").replace(" ",""))

                    }
                }
            }
        }
    }

    private fun rvSetUp(choose2:String) {

        leftClick = { i,s ->

            filter(i)

            if (type == "area") {
                left = s
                rightAdapter?.updateData(rightData!!)
            }

        }

        rightClick = { i,s ->
            var string = s

            if (type != "type") {

                if (rightAdapter?.choose?.contains(s) == false) {

                    if (s == "UIorUX") {
                        string = s.replace("or","/")
                    }

                    if (rightAdapter?.choose == "") {
                        rightAdapter?.choose = s
                    } else {
                        rightAdapter?.choose = rightAdapter?.choose + ",$s"
                    }

                    data.add(string)

                    if (s.contains("全區")) {
                        //區域選到全區時，add該區所有區域

                        rightData?.forEach {
                            if (rightAdapter?.choose?.contains(it) == false) {

                                if (rightAdapter?.choose == "") {
                                    rightAdapter?.choose = it
                                } else {
                                    rightAdapter?.choose = rightAdapter?.choose + ",$it"
                                }

                                data.add(it)
                            }
                        }
                    }
                } else {

                    if (s == "UIorUX") {
                        string = s.replace("or","/")
                    }

                    if (rightAdapter?.choose?.contains(",") == true) {
                        rightAdapter?.choose = rightAdapter?.choose?.replace(",$s", "") ?: ""
                    } else {
                        rightAdapter?.choose = rightAdapter?.choose?.replace(s, "") ?: ""
                    }

                    data.remove(string)
                }
            } else {
                //種類單選
                rightAdapter?.choose = s
                data2 = string
            }

            if (s == "全區" && left == "全部地點") {
                rightAdapter?.choose = "全區"
                data = mutableListOf()
            }

            rightAdapter?.pos = i
            rightAdapter?.notifyDataSetChanged()

        }

        if (type == "area") {
            leftAdapter = TypeLeftAdapter("",leftData!!,context!!,null,"",leftClick)
            rv_fragment_type.adapter = leftAdapter
            rightAdapter = TypeRightAdapter("",true,rightData,context!!,null,choose2,rightClick)
        } else {
            rightAdapter = TypeRightAdapter("",false,rightData,context!!,null,choose2,rightClick)
        }

        rv2_fragment_type.adapter = rightAdapter

    }

    private fun filter(position:Int?) {
        when (type) {
            "area" -> {
                when (position) {
                    0 -> rightData = typeAreaModel?.Taipei
                    1 -> rightData = typeAreaModel?.NewTaipei
                    2 -> rightData = typeAreaModel?.Taoyuan
                    3 -> rightData = typeAreaModel?.XinzhuCity
                    4 -> rightData = typeAreaModel?.XinzhuCounty
                    5 -> rightData = typeAreaModel?.Miaoli
                    6 -> rightData = typeAreaModel?.Taizhong
                    7 -> rightData = typeAreaModel?.Zhanghua
                    8 -> rightData = typeAreaModel?.Nantou
                    9 -> rightData = typeAreaModel?.Yunlin
                    10 -> rightData = typeAreaModel?.ChiayiCity
                    11 -> rightData = typeAreaModel?.ChiayiCounty
                    12 -> rightData = typeAreaModel?.Tainan
                    13 -> rightData = typeAreaModel?.Kaohsiung
                    14 -> rightData = typeAreaModel?.Pingtung
                    15 -> rightData = typeAreaModel?.Keelung
                    16 -> rightData = typeAreaModel?.Yilan
                    17 -> rightData = typeAreaModel?.Hualien
                    18 -> rightData = typeAreaModel?.Taitung
                    19 -> rightData = typeAreaModel?.Penghu
                    20 -> rightData = typeAreaModel?.Kinmen
                    21 -> rightData = typeAreaModel?.Lienchiang
                    22 -> rightData = typeAreaModel?.all
                    23 -> rightData = typeAreaModel?.remote
                }
            }
            "type" -> {
                if (from == 0) {
                    when (position) {
                        0 -> rightData = typeModel?.all
                        1 -> rightData = typeModel?.art
                        2 -> rightData = typeModel?.music
                        3 -> rightData = typeModel?.media
                        4 -> rightData = typeModel?.writer
                        5 -> rightData = typeModel?.programing
                        6 -> rightData = typeModel?.language
                        7 -> rightData = typeModel?.consultation
                        8 -> rightData = typeModel?.love
                        9 -> rightData = typeModel?.beauty
                        10 -> rightData = typeModel?.smart
                        11 -> rightData = typeModel?.life
                        12 -> rightData = typeModel?.show
                        13 -> rightData = typeModel?.divine
                        14 -> rightData = typeModel?.craft
                        15 -> rightData = typeModel?.farmer
                        16 -> rightData = typeModel?.service
                        17 -> {
                            rightData = null
                            type3 = "其他"
                        }
                    }
                } else {
                    when (position) {
                        0 -> rightData = findModel?.teach
                        1 -> rightData = findModel?.service
                        2 -> rightData = findModel?.group
                    }
                }
            }
            "typeDetail" -> {
                if(from == 0) {
                    when (mainType2) {
                        "" -> rightData = null
                        "全部類別" -> rightData = typeModel?.all
                        "美術繪畫" -> rightData = typeModel?.art
                        "音樂樂器" -> rightData = typeModel?.music
                        "多媒體" -> rightData = typeModel?.media
                        "文筆寫作" -> rightData = typeModel?.writer
                        "程式設計" -> rightData = typeModel?.programing
                        "語文" -> rightData = typeModel?.language
                        "諮商" -> rightData = typeModel?.consultation
                        "感情" -> rightData = typeModel?.love
                        "美容時尚" -> rightData = typeModel?.beauty
                        "益智" -> rightData = typeModel?.smart
                        "生活" -> rightData = typeModel?.life
                        "運動" -> rightData = typeModel?.sport
                        "表演" -> rightData = typeModel?.show
                        "命理" -> rightData = typeModel?.divine
                        "工藝" -> rightData = typeModel?.craft
                        "農藝" -> rightData = typeModel?.farmer
                        "服務" -> rightData = typeModel?.service
                        "其他類別" -> {
                            rightData = null
                            type3 = "其他"
                        }
                    }
                } else {
                    when(mainType2) {
                        "" -> rightData = null
                        "找教學" -> rightData = findModel?.teach
                        "找服務" -> rightData = findModel?.service
                        "揪團" -> rightData = findModel?.group
                    }
                }
            }
        }
    }
}