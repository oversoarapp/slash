package com.oversoar.slash.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.firebase.database.*
import com.google.gson.Gson
import com.oversoar.slash.Commom
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.CustomDialog
import com.oversoar.slash.Model.LatestMessageModel
import com.oversoar.slash.Model.MessageModel
import com.oversoar.slash.Model.UserDBModel
import com.oversoar.slash.R
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.item_message_list.view.*

class MessageListFragment : Fragment() {

    private var nAdapter = GroupAdapter<ViewHolder>()
    private var messageHashMap = HashMap<String,MessageModel>()
    private var dialog:CustomDialog? = null
    private var blockLists:ArrayList<String>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        setData()

    }

    private fun initView() {
        rv_fragment_list.adapter = nAdapter
        rv_fragment_list.addItemDecoration(DividerItemDecoration(context,DividerItemDecoration.VERTICAL))

        val deleteClick:(String)->Unit = {
            val msgRef = FirebaseDatabase.getInstance().getReference("/messages/${Commom.userInfo?.user_id}/$it")
            val latestMsgRef = FirebaseDatabase.getInstance().getReference("/latest_message/${Commom.userInfo?.user_id}/$it")
            msgRef.removeValue()
            latestMsgRef.removeValue()
            dialog?.cancel()
        }

        val blockClick:(String)->Unit = {
            if (blockLists?.contains(it) == false) {
                blockLists?.add(it)
                val ref = FirebaseDatabase.getInstance().getReference("/users/${userInfo?.user_id}/block")
                ref.setValue(blockLists)
            }

            dialog?.cancel()
        }

        nAdapter.setOnItemClickListener { item, view ->

            val listViewHolder = item as MessageListViewHolder

            val fg = ChatFragment()
            val bundle = Bundle()
            bundle.putString("chatTo", Gson().toJson(listViewHolder.chatPartnerUser))
            bundle.putString("from","messageList")
            fg.arguments = bundle
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.addToBackStack(null)
            ft?.replace(R.id.fragment_container, fg)?.commit()

            Log.i("chatPartnerUser", Gson().toJson(listViewHolder.chatPartnerUser))

        }

        nAdapter.setOnItemLongClickListener { item, view ->

            val viewHolder = item as MessageListViewHolder
            val id = viewHolder.chatPartnerUser?.user_id?:""

            dialog = CustomDialog(context!!,id,blockClick,deleteClick)
            dialog?.show()

            return@setOnItemLongClickListener false
        }
    }

    private fun refreshRvMessage() {
        nAdapter.clear()

        if (messageHashMap.size == 0 ) {
            rv_fragment_list?.visibility = View.INVISIBLE
            empty_list?.visibility = View.VISIBLE
            empty_list?.text = "您尚無任何訊息"
        } else {
            rv_fragment_list?.visibility = View.VISIBLE
            messageHashMap.values.mapIndexed { index, it ->
                val keys = messageHashMap.keys.toMutableList()
                if (blockLists?.contains(keys[index]) == false) {
                    nAdapter.add(MessageListViewHolder(keys[index], it))
                }
            }

            if (blockLists?.size?:0 == 1) {
                empty_list?.visibility = View.VISIBLE
                empty_list?.text = "您尚無任何訊息"
            } else {
                empty_list?.visibility = View.INVISIBLE
            }
        }
    }

    private fun setData() {
        //find blocklist
        val ref = FirebaseDatabase.getInstance().getReference("/users/${userInfo?.user_id}")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val userDBModel = snapshot.getValue(UserDBModel::class.java)?:return
                blockLists = userDBModel.block
                //getdata
                listenForLatestMessages()
            }
        })
    }

    private fun listenForLatestMessages() {
        val ref = FirebaseDatabase.getInstance().getReference("/latest_message/${userInfo?.user_id}")
        ref.keepSynced(true)

        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(error: DatabaseError) {
                rv_fragment_list.visibility = View.INVISIBLE
                empty_list.visibility = View.VISIBLE
                empty_list.text = "您尚無任何訊息"
            }
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val message = snapshot.getValue(MessageModel::class.java)?:return

                if (blockLists?.contains(snapshot.key?: "") == false) {
                    messageHashMap[snapshot.key ?: ""] = message
                }

                refreshRvMessage()

            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val message = snapshot.getValue(MessageModel::class.java)?:return

                if (blockLists?.contains(snapshot.key?: "") == false) {
                    messageHashMap[snapshot.key ?: ""] = message
                }

                refreshRvMessage()

            }
            override fun onChildRemoved(snapshot: DataSnapshot) {

                messageHashMap.remove(snapshot.key)
                refreshRvMessage()

            }
        })

    }


    inner class MessageListViewHolder(val chatId:String ,val chatMessage:MessageModel) : Item<ViewHolder>() {

        var chatPartnerUser: UserDBModel? = null

        override fun getLayout(): Int {
            return R.layout.item_message_list
        }

        override fun bind(viewHolder: ViewHolder, position: Int) {

            val lref = FirebaseDatabase.getInstance().getReference("/latest_message/${userInfo?.user_id}/$chatId")
            lref.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    val lmsg = snapshot.getValue(LatestMessageModel::class.java)
                    if (lmsg?.count != 0) {
                        viewHolder.itemView.item_message_list_count.visibility = View.VISIBLE
                        viewHolder.itemView.item_message_list_count.text = lmsg?.count.toString()
                    } else {
                        viewHolder.itemView.item_message_list_count.visibility = View.GONE
                    }
                }
            })

            val ref = FirebaseDatabase.getInstance().getReference("/users/$chatId")
            ref.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                }

                override fun onDataChange(snapshot: DataSnapshot) {

                    chatPartnerUser = snapshot.getValue(UserDBModel::class.java)

                    viewHolder.itemView.item_message_list_message.text = chatMessage.text
                    viewHolder.itemView.item_message_list_name.text = chatPartnerUser?.user_name

                    if (chatPartnerUser?.photoURL?.isEmpty() == true) {
                        Picasso.get().load(R.mipmap.ic_launcher).fit()
                            .into(viewHolder.itemView.item_message_list_photo)
                    } else {
                        Picasso.get().load(chatPartnerUser?.photoURL).placeholder(R.drawable.ic_person_black_24dp)
                                .fit()
                                .centerInside()
                                .into(viewHolder.itemView.item_message_list_photo)
                    }
                }
            })
        }
    }
}
