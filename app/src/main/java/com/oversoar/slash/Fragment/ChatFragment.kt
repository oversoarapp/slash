package com.oversoar.slash.Fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.firebase.auth.UserInfo
import com.google.firebase.database.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Model.LatestMessageModel
import com.oversoar.slash.Model.MessageModel
import com.oversoar.slash.Model.UserDBModel
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.item_chat_left.view.*
import kotlinx.android.synthetic.main.item_chat_right.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class ChatFragment : Fragment() {

    private var nAdapter = GroupAdapter<ViewHolder>()
    private var toUser: UserDBModel? = null
    private var blockLists:ArrayList<String>? = null
    private var count = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        if (arguments != null) {

            //TODO 店舖業務邏輯待定
            if (arguments?.getString("from") != "messageList" ) {
                //pass id
                val toUserId = arguments?.getString("chatTo")
                getData(toUserId ?: "")
            } else {
                //pass object
                val typeToken = object : TypeToken<UserDBModel>(){}.type
                toUser = Gson().fromJson(arguments?.getString("chatTo"),typeToken)
                val actionBar = (activity as AppCompatActivity?)!!.supportActionBar
                actionBar?.show()
                actionBar?.title = toUser?.user_name?:""
                rv_fragment_chat.adapter = nAdapter

                val ref = FirebaseDatabase.getInstance().getReference("/messages/${userInfo?.user_id}/${toUser?.user_id?:""}")
                ref.keepSynced(true)

                listenForFirebase()

            }

            getBlockLists()

        }

        chat_log_send.setOnClickListener {
            if (chat_log_tv.text.isNotEmpty() && toUser?.user_id?.isNotEmpty() == true) {
                saveMessage()
            }
        }

    }

    private fun count() {

        val lref = FirebaseDatabase.getInstance().getReference("/latest_message/${userInfo?.user_id}/${toUser?.user_id}/count")
        lref.setValue(0)

        val llref = FirebaseDatabase.getInstance().getReference("/latest_message/${toUser?.user_id}/${userInfo?.user_id}")
        //傳給別人的
        llref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val latestMessageModel = dataSnapshot.getValue(LatestMessageModel::class.java)
                count = latestMessageModel?.count?:0
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

    private fun getData(id:String) {

        progressBar10.visibility = View.VISIBLE

        val ref = FirebaseDatabase.getInstance().getReference("/users/$id")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                progressBar10?.visibility = View.INVISIBLE

                toUser = dataSnapshot.getValue(UserDBModel::class.java)
                val actionBar = (activity as AppCompatActivity?)!!.supportActionBar
                actionBar?.show()
                actionBar?.title = toUser?.user_name?:""
                rv_fragment_chat.adapter = nAdapter

                val ref = FirebaseDatabase.getInstance().getReference("/messages/${userInfo?.user_id}/${toUser?.user_id?:""}")
                ref.keepSynced(true)

                listenForFirebase()
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

    private fun listenForFirebase() {

        count()

        val ref = FirebaseDatabase.getInstance().getReference("/messages/${userInfo?.user_id}/${toUser?.user_id}")
        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val messages = snapshot.getValue(MessageModel::class.java)?:return
                if(messages.from_id == userInfo?.user_id) {
                    val mUserInfo = UserDBModel(userInfo?.user_id?:"", userInfo?.user_name?:"", userInfo?.image_URL?:"", arrayListOf())
                    nAdapter.add(ChatRightItem(messages.text, mUserInfo))
                } else {
                    nAdapter.add(ChatLeftItem(messages.text, toUser?:return ))
                }
                rv_fragment_chat?.scrollToPosition(nAdapter.itemCount -1)
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
            }

        })
    }

    private fun getBlockLists() {
        //chat user's block lists
        val ref = FirebaseDatabase.getInstance().getReference("/users/${toUser?.user_id}")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val userDBModel = snapshot.getValue(UserDBModel::class.java)?:return
                blockLists = userDBModel.block
            }
        })
    }

    @SuppressLint("SimpleDateFormat")
    private fun saveMessage() {
        val text = chat_log_tv.text.toString()
        val ref = FirebaseDatabase.getInstance().getReference("/messages/${userInfo?.user_id}/${toUser?.user_id}").push()
        val toRef = FirebaseDatabase.getInstance().getReference("/messages/${toUser?.user_id}/${userInfo?.user_id}").push()

        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val time = df.format(Date().time)
        val messageModel = MessageModel(time.toString(),text, userInfo?.user_id?:"")

        toRef.setValue(messageModel)
        ref.setValue(messageModel)
            .addOnSuccessListener {
                chat_log_tv.text.clear()
                rv_fragment_chat.scrollToPosition(nAdapter.itemCount-1)

                if (blockLists?.contains(userInfo?.user_id) == false) {
                    CoroutineScope(Dispatchers.IO).launch {
                        ApiClient().createService(ApiService::class.java).sendMsg(userInfo?.user_name
                                ?: "", text, toUser?.user_id ?: "")
                    }
                }

                Log.i("ChatFragment","save message successfully")
            }

        var mMessageModel = LatestMessageModel(0,time,text, userInfo?.user_id?:"")
        val latestMessage = FirebaseDatabase.getInstance().getReference("/latest_message/${userInfo?.user_id}/${toUser?.user_id}")
        latestMessage.setValue(mMessageModel)

        //對方未讀訊息+1

        mMessageModel = LatestMessageModel(count+1,time,text, userInfo?.user_id?:"")
        val mLatestMessage = FirebaseDatabase.getInstance().getReference("/latest_message/${toUser?.user_id}/${userInfo?.user_id}")
        mLatestMessage.setValue(mMessageModel)
    }

    class ChatLeftItem(val text:String,val userModel: UserDBModel): Item<ViewHolder>() {
        override fun getLayout(): Int {
            return R.layout.item_chat_left
        }

        override fun bind(viewHolder: ViewHolder, position: Int) {

            viewHolder.itemView.item_chat_left_text.text = text

            if (userModel.photoURL.isEmpty()) {
                Picasso.get().load(R.mipmap.ic_launcher).fit()
                    .into(viewHolder.itemView.item_chat_left_photo)
            } else {
                Picasso.get().load(userModel.photoURL)
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.ic_person_black_24dp)
                    .into(viewHolder.itemView.item_chat_left_photo)

            }

        }
    }

    class ChatRightItem(val text:String,val userModel: UserDBModel): Item<ViewHolder>() {
        override fun getLayout(): Int {
            return R.layout.item_chat_right
        }

        override fun bind(viewHolder: ViewHolder, position: Int) {

            viewHolder.itemView.item_chat_right_text.text = text

            if (userModel.photoURL.isEmpty()) {
                Picasso.get().load(R.mipmap.ic_launcher).fit()
                    .into(viewHolder.itemView.item_chat_right_photo)
            } else {
                Picasso.get().load(userModel.photoURL).placeholder(R.drawable.ic_person_black_24dp)
                    .fit()
                    .centerInside()
                    .into(viewHolder.itemView.item_chat_right_photo)
            }

        }
    }
}
