package com.oversoar.slash.Fragment

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsClient.getPackageName
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.MainActivity
import com.oversoar.slash.R
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.item_set_list.view.*

class SetFragment: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {
        val click:(String)->Unit = {
            when (it) {
                "個人資料" -> {
                    if (!userInfo?.user_id.isNullOrEmpty()) {
                        val fg = EditInfoFragment()
                        val bundle = Bundle()
                        bundle.putString("from","editInfo")
                        fg.arguments = bundle
                        val ft = activity?.supportFragmentManager?.beginTransaction()
                        ft?.addToBackStack(null)
                        ft?.replace(R.id.fragment_container, fg)?.commit()
                    } else {
                        val ft = activity?.supportFragmentManager?.beginTransaction()
                        ft?.addToBackStack(null)
                        ft?.replace(R.id.fragment_container, GotoLoginFragment())?.commit()
                    }
                }
                "黑名單管理" -> {
                    if (!userInfo?.user_id.isNullOrEmpty()) {
                        val fg = BlockFragment()
                        val bundle = Bundle()
                        bundle.putString("from","editInfo")
                        fg.arguments = bundle
                        val ft = activity?.supportFragmentManager?.beginTransaction()
                        ft?.addToBackStack(null)
                        ft?.replace(R.id.fragment_container, fg)?.commit()
                    } else {
                        val ft = activity?.supportFragmentManager?.beginTransaction()
                        ft?.addToBackStack(null)
                        ft?.replace(R.id.fragment_container, GotoLoginFragment())?.commit()
                    }
                }
                "客服信箱" -> {
                    val ft = activity?.supportFragmentManager?.beginTransaction()
                    ft?.addToBackStack(null)
                    ft?.replace(R.id.fragment_container, FeedbackFragment())?.commit()
                }
                "相關規範" -> {
                    val fg = EditInfoFragment()
                    val bundle = Bundle()
                    bundle.putString("from","terms")
                    fg.arguments = bundle
                    val ft = activity?.supportFragmentManager?.beginTransaction()
                    ft?.addToBackStack(null)
                    ft?.replace(R.id.fragment_container, fg)?.commit()
                }
                "分享店舖" -> {
                    val appPackageName: String = context?.packageName ?:""

                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                    } catch (anfe: ActivityNotFoundException) {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                    }
                }
                "登出" -> {
                    if (!userInfo?.user_id.isNullOrEmpty()) {

                        userInfo = null

                        val sp = context?.getSharedPreferences("userData", Context.MODE_PRIVATE)
                        sp?.edit()?.clear()?.apply()

                        Toast.makeText(context,"登出成功！",Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        val nAdapter = GroupAdapter<ViewHolder>()
        rv_fragment_list.adapter = nAdapter
        rv_fragment_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        var list = listOf("個人資料","黑名單管理","客服信箱","相關規範","分享店鋪","登出")

        list.forEach {
            nAdapter.add(SetViewHolder(it,click))
        }

    }

    class SetViewHolder(var text:String,val click:(String)->Unit): Item<ViewHolder>() {

        override fun getLayout(): Int {
            return R.layout.item_set_list
        }

        override fun bind(viewHolder: ViewHolder, position: Int) {
            if (position == 0) {
                viewHolder.itemView.imageView3.visibility = View.GONE
            }

            viewHolder.itemView.tv_item_set_fragment.text = text

            viewHolder.itemView.setOnClickListener {
                click(text)
            }

        }
    }
}