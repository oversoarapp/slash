package com.oversoar.slash.Fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.oversoar.slash.LoginActivity
import com.oversoar.slash.R
import kotlinx.android.synthetic.main.fragment_gotologin.*

class GotoLoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_gotologin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_go_to_login.setOnClickListener {
            activity?.startActivity(Intent(context,
                LoginActivity::class.java))
        }

    }
}