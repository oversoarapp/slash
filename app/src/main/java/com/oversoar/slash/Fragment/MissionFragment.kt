package com.oversoar.slash.Fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Adapter.GridViewAdapter
import com.oversoar.slash.Adapter.GridViewAdapter2
import com.oversoar.slash.Commom
import com.oversoar.slash.Commom.type1
import com.oversoar.slash.Commom.type2
import com.oversoar.slash.Commom.type3
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Commom.userMissionFollow
import com.oversoar.slash.Model.*
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import com.oversoar.slash.TypeActivity
import kotlinx.android.synthetic.main.fragment_store.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import kotlin.math.max
import kotlin.math.sqrt

class MissionFragment : Fragment() {

    private val TAG = "MissionFragment"
    private var storage = Firebase.storage
    private lateinit var missionData: MissionInfoModel
    private var from = ""
    private var deleteGone = false
    private val SELECT_PHOTO = 111
    private var position = 0
    private var photoList = mutableListOf<String>("","","","")
    private var chooseClick: ((Int) -> Unit)? = null
    private var deleteClick: ((Int) -> Unit)? = null
    private var nAdapter: GridViewAdapter? = null
    private var mAdapter: GridViewAdapter2? = null
    private var blockLists: ArrayList<String>? = null

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_store, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBlockLists()
        initView()

    }

    override fun onResume() {
        super.onResume()

        et_name_fragment_store.setText(missionData.mission_name)
        et_content_ragment_store.setText(missionData.mission_detail)
        area_btn_store.text = type1?:missionData.location
        skills_btn_store.text = type2?:missionData.find
        type_btn_store.text = type3?:missionData.find_detail

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode){
            SELECT_PHOTO -> {
                if (resultCode == Activity.RESULT_OK && data != null) {

                    photoList[position] = data.data.toString()

                    if (deleteGone) {
                        nAdapter?.notifyDataSetChanged()
                    } else {
                        mAdapter?.notifyDataSetChanged()
                    }

                }
            }
        }
    }

    private fun initView() {

        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.title = "任務資訊"
        follow_btn_fragment_store.text = "追蹤任務"

        chooseClick = {
            position = it
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            startActivityForResult(intent, SELECT_PHOTO)
        }

        deleteClick = {
            photoList[it] = ""

            if (deleteGone) {
                nAdapter?.notifyDataSetChanged()
            } else {
                mAdapter?.notifyDataSetChanged()
            }
        }

        if (arguments != null) {
            if (arguments!!.getString("missionData","").isNotEmpty()) {
                val listType = object : TypeToken<MissionInfoModel>() {}.type
                missionData = Gson().fromJson(arguments?.getString("missionData"), listType)
            }

            if (arguments!!.getString("from","").isNotEmpty()) {
                from = arguments!!.getString("from","")
            }

        }

        when (from) {
            "","Main"-> {

                if (userInfo?.user_id == missionData?.user_id) {
                    follow_btn_fragment_store.background = context?.getDrawable(R.drawable.rounded_gray_shape)
                    chat_btn_fragment_store.background = context?.getDrawable(R.drawable.rounded_gray_shape)
                }

                if (blockLists?.contains(missionData?.user_id) == true) {
                    chat_btn_fragment_store.background = context?.getDrawable(R.drawable.rounded_gray_shape)
                }

                new_btn_store.visibility = View.GONE
                linearLayout3.visibility = View.VISIBLE
                deleteGone = true
                chooseClick = null
                deleteClick = null
            }
            "Focus" -> {

                if (blockLists?.contains(missionData?.user_id) == true) {
                    chat_btn_fragment_store.background = context?.getDrawable(R.drawable.rounded_gray_shape)
                }

                new_btn_store.visibility = View.GONE
                linearLayout3.visibility = View.VISIBLE
                follow_btn_fragment_store.text = "取消追蹤"
                deleteGone = true
                chooseClick = null
                deleteClick = null
            }
            "user"-> {
                new_btn_store.visibility = View.VISIBLE
                linearLayout3.visibility = View.GONE
                et_name_fragment_store.isEnabled = true
                et_content_ragment_store.isEnabled = true
            }
        }

        setUp()

        area_btn_store.setOnClickListener {

            val intent = Intent(context, TypeActivity::class.java)
            intent.putExtra("ChooseType","area")
            intent.putExtra("from","mission")
            intent.putExtra("data",area_btn_store.text.toString())

            startActivity(intent)

        }

        skills_btn_store.setOnClickListener {

            val intent = Intent(context, TypeActivity::class.java)
            intent.putExtra("ChooseType","type")
            intent.putExtra("from","mission")
            intent.putExtra("data",skills_btn_store.text.toString())

            startActivity(intent)

        }

        type_btn_store.setOnClickListener {

            val intent = Intent(context, TypeActivity::class.java)
            intent.putExtra("ChooseType","typeDetail")
            intent.putExtra("from","mission")
            intent.putExtra("data",skills_btn_store.text.toString())
            intent.putExtra("data2",type_btn_store.text.toString())

            startActivity(intent)

        }

        new_btn_store.setOnClickListener {

            val name = et_name_fragment_store.text.toString()
            val content = et_content_ragment_store.text.toString()
            val location = area_btn_store.text.toString()
            val find = skills_btn_store.text.toString()
            val findDetail = type_btn_store.text.toString()

            if (name.isNotEmpty() && content.isNotEmpty() && location != "地點" && find != "種類" && findDetail != "細項") {
                if (name != missionData.mission_name || content != missionData.mission_detail || location != missionData.location
                    || find != missionData.find || findDetail != missionData.find_detail
                ) {
                    editMission(name, content, location, find, findDetail)
                }
            } else {
                Toast.makeText(context,"欄位皆不可空白！",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            convertUpload()

        }

        follow_btn_fragment_store.setOnClickListener {

            if (userInfo != null) {
                if (userInfo?.user_id == missionData.user_id) {
                    return@setOnClickListener
                }

                when (follow_btn_fragment_store.text) {
                    "取消追蹤" -> followMission("1")
                    "追蹤店鋪" -> followMission("0")
                }

            } else {
                val ft = activity?.supportFragmentManager?.beginTransaction()
                ft?.replace(
                    R.id.fragment_container,
                    GotoLoginFragment()
                )?.commit()
            }

        }

        chat_btn_fragment_store.setOnClickListener {
            if (userInfo != null) {

                if (missionData.user_id == userInfo?.user_id) {
                    return@setOnClickListener
                }

                if (blockLists?.contains(missionData?.user_id) == true) {
                    Toast.makeText(context!!,"您已封鎖此用戶，欲解除封鎖請至設定的黑名單管理！",Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }

                val fg = ChatFragment()
                val bundle = Bundle()
                bundle.putString("chatTo", missionData.user_id)
                fg.arguments = bundle
                val ft = activity?.supportFragmentManager?.beginTransaction()
                ft?.replace(
                        R.id.fragment_container,
                        fg
                )?.commit()
            } else {
                val ft = activity?.supportFragmentManager?.beginTransaction()
                ft?.replace(
                        R.id.fragment_container,
                        GotoLoginFragment()
                )?.commit()
            }
        }
    }

    private fun getBlockLists() {
        val ref = FirebaseDatabase.getInstance().getReference("/users/${userInfo?.user_id}")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val userDBModel = snapshot.getValue(UserDBModel::class.java)?:return
                blockLists = userDBModel.block
            }
        })
    }

    private fun setUp() {

        if (deleteGone) {
            nAdapter = GridViewAdapter(context!!,photoList,chooseClick,deleteClick)
            gv_fragment_store.adapter = nAdapter
        } else {
            //照片可更動
            mAdapter = GridViewAdapter2(context!!,photoList,chooseClick,deleteClick)
            gv_fragment_store.adapter = mAdapter
        }

        if (missionData != null) {

            photoList[0] = missionData.image_URL_1?:""
            photoList[1] = missionData.image_URL_2?:""
            photoList[2] = missionData.image_URL_3?:""
            photoList[3] = missionData.image_URL_4?:""

            if (deleteGone) {
                nAdapter?.updateData(photoList)
            } else {
                mAdapter?.updateData(photoList)
            }

            userMissionFollow?.forEach {
                if (missionData?.id?:"" == it.id) {
                    follow_btn_fragment_store.text = "取消追蹤"
                    return@forEach
                }
            }

            et_name_fragment_store.setText(missionData.mission_name)
            et_content_ragment_store.setText(missionData.mission_detail)
            area_btn_store.text = type1?:missionData.location
            skills_btn_store.text = type2?:missionData.find
            type_btn_store.text = type3?:missionData.find_detail

        }
    }

    private fun convertUpload() {

        progressBar3?.visibility = View.VISIBLE

        photoList.forEachIndexed { index, s ->
            if (s.contains("content")) {
                val stream = context?.contentResolver?.openInputStream(Uri.parse(s))!!
                var bitmap = BitmapFactory.decodeStream(stream)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val byteArray = baos.toByteArray()

                uploadImg(index+1,missionData.id?:"",byteArray)

            } else {
                CoroutineScope(Dispatchers.IO).launch{
                    ApiClient().createService(ApiService::class.java)
                        .uploadMissionImg(
                            missionData.id?:"",
                            userInfo?.user_id ?: "",
                            index+1,
                            s
                        )
                    withContext(Dispatchers.Main) {
                        progressBar3?.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun uploadImg (index:Int,missionId:String,bytes:ByteArray) {
        //upload to firebase & aws

        progressBar3.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.IO).launch {
            if (bytes.isNotEmpty()) {
                val ref =
                    storage?.reference?.child("user${userInfo?.user_id ?: ""}/mission$missionId/detail$index.jpg")

                val uploadTask = ref?.putBytes(bytes)

                uploadTask?.continueWithTask { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    ref.downloadUrl
                }?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUri = task.result.toString()
                        CoroutineScope(Dispatchers.IO).launch {
                            ApiClient().createService(ApiService::class.java)
                                .uploadMissionImg(
                                    missionId,
                                    userInfo?.user_id ?: "",
                                    index,
                                    downloadUri
                                )
                            withContext(Dispatchers.Main) {
                                Toast.makeText(context, "照片修改成功！", Toast.LENGTH_SHORT).show()
                                progressBar3?.visibility = View.GONE
                            }
                        }
                    }
                }
            }
        }
    }

    private fun editMission(name:String, content:String, location:String, type:String, typeDetail:String) {

        progressBar3.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {
            var result: ApiResult<SingleMissionModel>
            var missionDataResult: ApiResult<MissionModel>
            withContext(Dispatchers.IO) {
                result = editMissionResponse(name, content, location, type, typeDetail)
                missionDataResult = getMissionData(userInfo?.user_id?:"")
                if (missionData.states == "3") {
                    ApiClient().createService(ApiService::class.java).updateMissionStates(missionData.id?:"","0")
                }
            }

            when (missionDataResult) {
                is ApiResult.Success -> {
                    Commom.userMissionData = (missionDataResult as ApiResult.Success<MissionModel>).data.result
                    Log.i(TAG, (missionDataResult as ApiResult.Success<MissionModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Log.i(TAG, (missionDataResult as ApiResult.Error).exception.toString())
                }
            }

            when (result) {
                is ApiResult.Success -> {

                    progressBar3?.visibility = View.GONE

                    Toast.makeText(context, "修改成功！", Toast.LENGTH_SHORT).show()
                    val fm = activity?.supportFragmentManager?.beginTransaction()
                    fm?.replace(R.id.fragment_container, StoreListFragment())?.commit()

                    Log.i(TAG, (result as ApiResult.Success<SingleMissionModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Toast.makeText(context, "修改失敗！", Toast.LENGTH_SHORT).show()
                    Log.i(TAG, (result as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private fun followMission(action: String) {

        progressBar3.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {
            var result: ApiResult<MissionModel>
            withContext(Dispatchers.IO) {
                result = followMissionResponse(action)
            }

            progressBar3.visibility = View.GONE

            when (result) {
                is ApiResult.Success -> {
                    if ((result as ApiResult.Success<MissionModel>).data.success == true) {
                        when (action) {
                            "0" -> {
                                //追蹤成功
                                follow_btn_fragment_store.text = "取消追蹤"
                            }
                            "1" -> {
                                //取消成功
                                follow_btn_fragment_store.text = "追蹤店鋪"
                            }
                        }
                    }
                    Log.i(TAG, (result as ApiResult.Success<MissionModel>).data.toString())
                }
                is ApiResult.Error -> {
                    Log.i(TAG, (result as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private suspend fun editMissionResponse(name:String, content:String, location:String, type:String, typeDetail:String): ApiResult<SingleMissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).saveMission(missionData.id?:"",name,content,location,type,typeDetail,
                    userInfo?.user_id?:"")
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

//    private suspend fun uploadImgResponse(storeId:String,userId:String,order:Int,img:String): ApiResult<StoreModel> {
//        return try {
//            val result = ApiClient().createService(ApiService::class.java).uploadStoreImg(storeId,userId,order,img)
//            ApiResult.Success(result)
//        } catch (e: Throwable) {
//            ApiResult.Error(e)
//        }
//    }

    private suspend fun followMissionResponse(action:String):ApiResult<MissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).followMission(userInfo?.user_id?:"",missionData.id?:"",action)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getMissionData(id:String): ApiResult<MissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getUserMission(id)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        Commom.type1 = null
        Commom.type2 = null
        Commom.type3 = null

    }

}