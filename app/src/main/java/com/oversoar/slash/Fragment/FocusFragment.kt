package com.oversoar.slash.Fragment

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Adapter.MissionListAdapter
import com.oversoar.slash.Adapter.StoreListAdapter
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.LoginActivity
import com.oversoar.slash.Model.*
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import kotlinx.android.synthetic.main.fragment_focus.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FocusFragment : Fragment() {

    private val TAG = "focusFragment"
    private var nAdapter: StoreListAdapter? = null
    private var mAdapter: MissionListAdapter? = null
    private lateinit var storeData: List<StoreInfoModel>
    private lateinit var missionData: List<MissionInfoModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_focus, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        getMissionList()

    }

    private fun initView() {

        val click:(Int)->Unit = {
            val bundle = Bundle()
            bundle.putString("storeData", Gson().toJson(storeData[it]))
            bundle.putString("from","Focus")
            val fm = StoreFragment()
            fm.arguments = bundle

            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.addToBackStack(null)
            ft?.replace(R.id.fragment_container, fm)?.commit()
        }

        val missionClick:(Int)->Unit = {
            val bundle = Bundle()
            bundle.putString("missionData", Gson().toJson(missionData[it]))
            bundle.putString("from","Focus")
            val fm = MissionFragment()
            fm.arguments = bundle

            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.addToBackStack(null)
            ft?.replace(R.id.fragment_container, fm)?.commit()
        }

        nAdapter = StoreListAdapter(context!!,"",click,null)
        mAdapter = MissionListAdapter(context!!,"",missionClick)
        rv_focus.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        tabLayout3.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tabPerform(tab?.position?:0)
            }
        })
    }

    private fun tabPerform(position:Int) {

        rv_focus.adapter = null

        when(position) {
            0 -> {
                getMissionList()
            }
            1 -> {
                if (userInfo?.user_id?.isEmpty() == true) {
                    AlertDialog.Builder(context)
                        .setTitle("訊息提示")
                        .setMessage("請先進行登入！")
                        .setPositiveButton("登入") {d,w ->
                            context?.startActivity(Intent(context,LoginActivity::class.java))
                        }
                        .setNegativeButton("取消") {d,w ->
                            d.cancel()
                        }
                } else {
                    getFollowStoreList()
                }
            }
        }
    }

    private fun getMissionList() {
        CoroutineScope(Dispatchers.Main).launch {
            var missionResult: ApiResult<MissionModel>
            withContext(Dispatchers.IO) {
                missionResult = getMissionListResponse()
            }

            when (missionResult) {
                is ApiResult.Success -> {
                    if ((missionResult as ApiResult.Success<MissionModel>).data.success == true && (missionResult as ApiResult.Success<MissionModel>).data.result != null) {
                        missionData = (missionResult as ApiResult.Success<MissionModel>).data.result!!
                        rv_focus?.adapter = mAdapter
                        mAdapter?.updateData((missionResult as ApiResult.Success<MissionModel>).data.result)
                    }
                    Log.i(TAG, (missionResult as ApiResult.Success<MissionModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Log.i(TAG, (missionResult as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private fun getFollowStoreList() {
        CoroutineScope(Dispatchers.Main).launch {
            var result: ApiResult<StoreModel>
            withContext(Dispatchers.IO) {
                result = getFollowStoreListResponse()
            }

            when (result) {
                is ApiResult.Success -> {
                    if ((result as ApiResult.Success<StoreModel>).data.success) {
                        storeData = (result as ApiResult.Success<StoreModel>).data.result
                        rv_focus?.adapter = nAdapter
                        nAdapter?.updateData((result as ApiResult.Success<StoreModel>).data.result)
                    }
                    Log.i(TAG, (result as ApiResult.Success<StoreModel>).data.toString())
                }

                is ApiResult.Error -> {
                    Log.i(TAG, (result as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private suspend fun getFollowStoreListResponse(): ApiResult<StoreModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).followStoreList(userInfo?.user_id?:"")
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getMissionListResponse():ApiResult<MissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).followMissionList(userInfo?.user_id?:"")
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

}
