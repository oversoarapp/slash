package com.oversoar.slash.Fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_editinfo.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import kotlin.math.max
import kotlin.math.sqrt


class EditInfoFragment : Fragment() {

    private val TAG = "EditInfoFragment"
    private var storage = Firebase.storage
    private val SELECT_PHOTO = 111
    private var byteArray = ByteArray(0)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_editinfo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode){
            SELECT_PHOTO -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val stream = context?.contentResolver?.openInputStream(data.data!!)
                    var bitmap = BitmapFactory.decodeStream(stream)
                    val baos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    byteArray = baos.toByteArray()

                    Picasso.get().load(data.data!!).into(circleImageView)

                }
            }
        }
    }

    private fun initView() {

        if (arguments != null && arguments?.getString("from","") != "") {
            if (arguments?.getString("from","") == "editInfo") {
                //使用者資料設定
                phone_et_editInfo.text = userInfo?.phone?:""
                name_editinfo.setText(userInfo?.user_name?:"")
                email_et_editInfo.text = userInfo?.google_email?: userInfo?.facebook_email

                if (userInfo?.image_URL != "") {
                    textView21.visibility = View.INVISIBLE
                    Picasso.get().load(userInfo?.image_URL).placeholder(R.mipmap.ic_launcher).fit().centerInside().into(circleImageView)
                } else {
                    textView21.visibility = View.VISIBLE
                    Picasso.get().load(R.drawable.ic_add_black_24dp).fit().centerInside().into(circleImageView)
                }
            } else {
                //相關規範
                edinfo_layout.visibility = View.GONE
                terms_view_logo.visibility = View.VISIBLE
                textView4.movementMethod = ScrollingMovementMethod.getInstance()
            }
        }

        circleImageView.setOnClickListener {
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            startActivityForResult(intent, SELECT_PHOTO)
        }

        pwd_change_editInfo.setOnClickListener {
            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.addToBackStack(null)
            ft?.replace(R.id.fragment_container,
                EditPwdFragment()
            )?.commit()
        }

        done_editInfo.setOnClickListener {
            if (byteArray.isNotEmpty()) {
                uploadImg(byteArray)
            }

            if (name_editinfo.text.toString().isNotEmpty() && name_editinfo.text.toString() != userInfo?.user_name) {
                changeUserName()
            }
        }

        name_editinfo.setOnEditorActionListener { textView, i, keyEvent ->
            if (i == EditorInfo.IME_ACTION_DONE ) {
                val view: View? = activity?.currentFocus
                if (view != null) {
                    val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                    imm?.hideSoftInputFromWindow(view.windowToken, 0)
                }
                false
            }
            true
        }
    }

    private fun uploadImg (bytes:ByteArray) {
        //upload to firebase & aws

        progressBar5.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.IO).launch {

            val ref = storage?.reference?.child("user${userInfo?.user_id?:""}/user.jpg")
            val uploadTask = ref?.putBytes(bytes)

            uploadTask?.continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                ref.downloadUrl
            }?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result.toString()
                    userInfo?.image_URL = downloadUri

                    CoroutineScope(Dispatchers.IO).launch {
                        ApiClient().createService(ApiService::class.java)
                            .uploadUserImage(
                                userInfo?.user_id ?: "",
                                downloadUri
                            )
                        withContext(Dispatchers.Main) {
                            progressBar5.visibility = View.GONE
                            Toast.makeText(context, "照片修改成功！", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }

    private fun changeUserName() {

        progressBar5.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.IO).launch {
            ApiClient().createService(ApiService::class.java)
                .changeUserName(userInfo?.user_id?:"",name_editinfo.text.toString())
            withContext(Dispatchers.Main) {
                userInfo?.user_name = name_editinfo.text.toString()
                progressBar5.visibility = View.GONE
                Toast.makeText(context, "資料修改成功！", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun compressionBitmap (bitmap: Bitmap, maxSize:Long):Bitmap? {
        try {
            var width = bitmap.width
            var height = bitmap.height
            val maxLength = max(width,height)
            if (maxLength > maxSize) {
                val scale = sqrt(maxLength / maxSize.toDouble())
                width = (width / scale).toInt()
                height = (height / scale).toInt()
            }
            return Bitmap.createScaledBitmap(bitmap,width,height,false)
        } catch (e:Throwable) {
            e.printStackTrace()
            return null
        }
    }

    private fun convertToBase64 (bitmap:Bitmap):String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

}
