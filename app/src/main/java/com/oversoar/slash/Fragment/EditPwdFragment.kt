package com.oversoar.slash.Fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Model.StringResultModel
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import kotlinx.android.synthetic.main.fragment_editpwd.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EditPwdFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_editpwd,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.title = "修改密碼"

        done_editpwd.setOnClickListener {

            val oldPwd = pwd_editpwd.text.toString()
            val newPwd = new_pwd_editpwd.text.toString()
            val rePwd = re_pwd_editpwd.text.toString()

            if (oldPwd == userInfo?.password?:"") {
                if (newPwd.length >= 6) {
                    if (newPwd == rePwd) {
                        changePwd(userInfo?.phone?:"",newPwd)
                    }
                } else {
                    Toast.makeText(context!!,"密碼長度需六碼以上！", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(context!!,"原密碼錯誤！", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun changePwd(phone: String,pwd:String) {

        progressBar6.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {
            var state: ApiResult<StringResultModel>
            withContext(Dispatchers.IO) {
                state = changePwdApi(phone,pwd)
            }

            progressBar6.visibility = View.GONE

            when (state) {
                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success) {
                        val sp = context!!.getSharedPreferences("userData",Context.MODE_PRIVATE)
                        sp.edit().putString("pwd",pwd).apply()
                        userInfo?.password = pwd

                        pwd_editpwd.text.clear()
                        new_pwd_editpwd.text.clear()
                        re_pwd_editpwd.text.clear()

                        Toast.makeText(context!!, "修改成功！", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(context!!,"修改失敗，請再試一次！", Toast.LENGTH_SHORT).show()
                    }
                }
                is ApiResult.Error -> {
                    AlertDialog.Builder(context!!)
                        .setTitle("訊息提示")
                        .setMessage("網路或系統有誤，請稍後再試！")
                        .show()
                }
            }
        }
    }

    private suspend fun changePwdApi (phone: String,pwd:String): ApiResult<StringResultModel>{
        return try {
            val result = ApiClient().createService(ApiService::class.java).changePassword(phone,pwd)
            ApiResult.Success(result)
        } catch (e:Exception) {
            ApiResult.Error(e)
        }
    }
}