package com.oversoar.slash.Fragment

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Model.StringResultModel
import com.oversoar.slash.Model.UserModel
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import kotlinx.android.synthetic.main.fragment_feedback.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Error

class FeedbackFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_feedback,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        done_feedback.setOnClickListener {
            val email = email_feedback.text.toString()
            val content = content_feedback.text.toString()
            val title = spinner_feedback.selectedItem.toString()

            sendFeedback(email,content,title)
        }
    }

    private fun sendFeedback (email:String,content:String,title:String) {

        progressBar9.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {
            var state: ApiResult<UserModel>
            withContext(Dispatchers.IO) {
                state = sendFeedbackResponse(email,content,title)
            }

            progressBar9.visibility = View.GONE

            when (state) {
                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success) {
                        email_feedback.setText("")
                        content_feedback.setText("")
                        Toast.makeText(context!!, "感謝您的建議！", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(context!!,"發送失敗，請再試一次！", Toast.LENGTH_SHORT).show()
                    }
                }
                is ApiResult.Error -> {
                    AlertDialog.Builder(context!!)
                            .setTitle("訊息提示")
                            .setMessage("網路或系統有誤，請稍後再試！")
                            .show()
                    Log.i("Feedback", (state as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private suspend fun sendFeedbackResponse(email:String,content:String,title:String): ApiResult<UserModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).sendMail(email,content,userInfo?.user_name?:"",title)
            ApiResult.Success(result)
        } catch (e:Exception) {
            ApiResult.Error(e)
        }
    }
}