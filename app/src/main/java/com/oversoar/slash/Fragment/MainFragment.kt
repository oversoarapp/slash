package com.oversoar.slash.Fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.SearchView
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Adapter.MissionListAdapter
import com.oversoar.slash.Adapter.StoreListAdapter
import com.oversoar.slash.Commom.mainType2
import com.oversoar.slash.Commom.search
import com.oversoar.slash.Commom.typeList1
import com.oversoar.slash.Commom.typeList3
import com.oversoar.slash.Model.MissionInfoModel
import com.oversoar.slash.Model.MissionModel
import com.oversoar.slash.Model.StoreInfoModel
import com.oversoar.slash.Model.StoreModel
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.*

class MainFragment : Fragment() {

    private val TAG = "MainFragment"
    private var page = 0
    private var job: Job? = null
    private var nAdapter: StoreListAdapter? = null
    private var mAdapter: MissionListAdapter? = null
    private var storeData: List<StoreInfoModel>? = null
    private var missionData: List<MissionInfoModel>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setData()
        initView()

    }

    private fun initView() {

        val click:(Int)->Unit = {
            val fm = StoreFragment()
            val bundle = Bundle()
            bundle.putString("from","Main")
            bundle.putString("storeData", Gson().toJson(storeData!![it]))
            fm.arguments = bundle

            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.fragment_container, fm)
            ft?.addToBackStack(null)
            ft?.commit()
        }

        val missionClick:(Int)->Unit = {
            val fm = MissionFragment()
            val bundle = Bundle()
            bundle.putString("from","Main")
            bundle.putString("missionData", Gson().toJson(missionData!![it]))
            fm.arguments = bundle

            val ft = activity?.supportFragmentManager?.beginTransaction()
            ft?.replace(R.id.fragment_container, fm)
            ft?.addToBackStack(null)
            ft?.commit()
        }

        val refresh:()->Unit = {
            page += 1
            getStoreData(search?:"","$page", typeList1?: listOf(),mainType2?:"", typeList3?: listOf(""))
        }

        textView.text = "地區(${typeList1?.size?:0})"
        textView2.text = mainType2?:"類別"
        textView3.text = "細項(${typeList3?.size?:0})"
        searchView_main.setText(search)
        nAdapter = StoreListAdapter(context!!,"",click,refresh)
        mAdapter = MissionListAdapter(context!!,"",missionClick)
        rv_fragment_main.adapter = nAdapter
        rv_fragment_main.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        searchView_main.addTextChangedListener (object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                search = text.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })

        searchView_main.setOnEditorActionListener { textView, i, keyEvent ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                if (!mainType2.isNullOrEmpty() && typeList3?.size == 0 || typeList3 == null) {
                    if (arguments?.getInt("from")?:0 == 0 ) {
                        getStoreData(search?:"","$page", typeList1?: listOf(""), "", listOf(""))
                    } else {
                        getMissionData(search?:"","$page", typeList1?: listOf(""),"",typeList3?: listOf(""))
                    }
                    Log.i("success", mainType2?:"empty"+typeList3.toString())
                } else {
                    if (arguments?.getInt("from")?:0 == 0 ) {
                        getStoreData(search?:"", "$page", typeList1?: listOf(""), mainType2?: "", typeList3?: listOf(""))
                    } else {
                        getMissionData(search?:"","$page", typeList1?: listOf(""),mainType2?: "",typeList3?: listOf(""))
                    }
                    Log.i("fail", mainType2?:"empty"+typeList3.toString())
                }
                false
            }
            true
        }

        textView.setOnClickListener {
            val fg = TypesFragment()
            val bundle = Bundle()
            bundle.putString("ChooseType","area")
            bundle.putInt("from",tabLayout?.selectedTabPosition?:0)
            fg.arguments = bundle
            val fm = activity?.supportFragmentManager?.beginTransaction()
            fm?.addToBackStack(null)
            fm?.replace(R.id.fragment_container, fg)?.commit()
        }

        textView2.setOnClickListener {
            val fg = TypesFragment()
            val bundle = Bundle()
            bundle.putString("ChooseType","type")
            bundle.putInt("from",tabLayout?.selectedTabPosition?:0)
            fg.arguments = bundle
            val fm = activity?.supportFragmentManager?.beginTransaction()
            fm?.addToBackStack(null)
            fm?.replace(R.id.fragment_container, fg)?.commit()
        }

        textView3.setOnClickListener {

            if (mainType2.isNullOrEmpty()) {
                Toast.makeText(context,"請先選擇一項類別！",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val fg = TypesFragment()
            val bundle = Bundle()
            bundle.putString("ChooseType","typeDetail")
            bundle.putInt("from",tabLayout?.selectedTabPosition?:0)
            fg.arguments = bundle
            val fm = activity?.supportFragmentManager?.beginTransaction()
            fm?.addToBackStack(null)
            fm?.replace(R.id.fragment_container, fg)?.commit()
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
                tabPerform(tab?.position?:0)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tabPerform(tab?.position?:0)
            }

        })

    }

    private fun setData() {
        if (arguments?.getInt("from") != null) {
            tabLayout.selectTab(tabLayout.getTabAt(arguments?.getInt("from")?:0))
        }

        if (typeList3 != null) {
            for (i in typeList3?.indices!!) {
                if (typeList3!![i] == "全部細項") {
                    typeList3 = null
                    break
                }
            }
        }

        if (typeList1 != null) {
            for (i in typeList1?.indices!!) {
                if (typeList1!![i] == "全區") {
                    typeList1 = null
                    break
                }
            }
        }

        if (typeList3.isNullOrEmpty() && mainType2.isNullOrEmpty() || mainType2 == "全部類別") {
            if (arguments?.getInt("from")?:0 == 0 ) {
                getStoreData(search?:"","$page", typeList1?: listOf(""), "", listOf(""))
            } else {
                getMissionData(search?:"","$page", typeList1?: listOf(""),"",typeList3?: listOf(""))
            }
        } else {
            if (!mainType2.isNullOrEmpty()) {
                if (arguments?.getInt("from") ?: 0 == 0) {
                    getStoreData(search?:"", "$page", typeList1?: listOf(""), mainType2 ?: "", typeList3?: listOf(""))
                } else {
                    getMissionData(search?:"", "$page", typeList1 ?: listOf(""), mainType2 ?: "", typeList3
                        ?: listOf(""))
                }
            }
        }
    }

    private fun tabPerform(position:Int) {

        rv_fragment_main.adapter = null

        when (position) {
            0 -> {
                if (!mainType2.isNullOrEmpty() && typeList3?.size == 0 || typeList3 == null) {
                    getStoreData(search?:"","$page", typeList1?: listOf(""), "", listOf(""))
                } else {
                    getStoreData(search?:"", "$page", typeList1 ?: listOf(""), mainType2 ?: "", typeList3?: listOf(""))
                }
            }
            1 -> {
                if (!mainType2.isNullOrEmpty() && typeList3?.size == 0 || typeList3 == null) {
                    getMissionData(search?:"","$page", listOf(""),"",listOf(""))
                } else {
                    getMissionData(search?:"", "$page", typeList1 ?: listOf(""), mainType2 ?: "", typeList3 ?: listOf(""))
                }
            }
        }

    }

    private fun getStoreData(search:String,page:String,location:List<String>,type:String,typeDetail:List<String>) {

        progressBar2?.visibility = View.VISIBLE

        job = CoroutineScope(Dispatchers.Main).launch {
            var state: ApiResult<StoreModel>
            withContext(Dispatchers.IO) {
                state = getStoreDataResponse(search, page, location, type, typeDetail)
                Log.i("searchData","search:$search,page:$page,location:$location,type:$type,typedeatil:$typeDetail")
            }

            progressBar2?.visibility = View.GONE

            when (state) {
                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success && !(state as ApiResult.Success<StoreModel>).data.result.isNullOrEmpty()) {
                        storeData = if (page != "0") {
                            val oldDataJson = Gson().toJson(storeData)
                            val newDataJson = Gson().toJson((state as ApiResult.Success).data.result)
                            val merge = "$oldDataJson$newDataJson"

                            val typeToken = object : TypeToken<List<StoreInfoModel>>(){}.type
                            Gson().fromJson<List<StoreInfoModel>>(merge,typeToken)

                        } else {
                            (state as ApiResult.Success).data.result
                        }
                        rv_fragment_main?.adapter = nAdapter
                        nAdapter?.updateData(storeData)
                    } else {
                        storeData = null
                        nAdapter?.updateData(storeData)
                    }
                    Log.i(TAG, (state as ApiResult.Success<StoreModel>).data.toString())
                }
                is ApiResult.Error -> {
                    Log.i(TAG, (state as ApiResult.Error).exception.message!!)
                }
            }
        }
    }

    private fun getMissionData(search:String,page:String,location:List<String>,find:String,findDetail:List<String>) {
        progressBar2.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {
            var state: ApiResult<MissionModel>
            withContext(Dispatchers.IO) {
                state = getMissionResponse(search, page, location, find, findDetail)
            }

            progressBar2.visibility = View.GONE

            when (state) {
                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success == true && !(state as ApiResult.Success<MissionModel>).data.result.isNullOrEmpty()) {
                        //有response
                        missionData = (state as ApiResult.Success).data.result
                        rv_fragment_main.adapter = mAdapter
                        mAdapter?.updateData(missionData)
                    } else {
                        missionData = null
                        mAdapter?.updateData(missionData)
                    }
                    Log.i(TAG, (state as ApiResult.Success).data.result.toString())
                }
                is ApiResult.Error -> {
                    Log.i(TAG, (state as ApiResult.Error).exception.toString())
                }
            }
        }
    }

    private suspend fun getStoreDataResponse (search:String,page:String,location:List<String>,type:String,typeDetail:List<String>): ApiResult<StoreModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getStoreData(search,page,location,type,typeDetail)
            ApiResult.Success(result)
        } catch (e:Exception) {
            ApiResult.Error(e)
        }
    }

    private suspend fun getMissionResponse (search:String,page:String,location:List<String>,find:String,findDetail:List<String>): ApiResult<MissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getMissionData(search,page,location,find,findDetail)
            ApiResult.Success(result)
        } catch (e:Exception) {
            ApiResult.Error(e)
        }
    }

}
