package com.oversoar.slash.Fragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.oversoar.slash.Model.MissionInfoModel
import com.oversoar.slash.Model.StoreInfoModel
import com.oversoar.slash.R
import com.oversoar.slash.Remote.ApiUrl.hostUrl
import com.oversoar.slash.Remote.ApiUrl.sendOrder
import kotlinx.android.synthetic.main.fragment_webview.*


class WebViewFragment: Fragment() {

    private var from = ""
    private var dataJson = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_webview, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initView() {

        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.hide()

        webSetting()

        if (arguments != null && arguments?.getString(
                "item",
                ""
            ) != "" && arguments?.getString("data", "") != ""
        ) {

            from = arguments?.getString("item", "") ?: ""
            dataJson = arguments?.getString("data", "") ?: ""

            if (from == "1") {
                val type = object : TypeToken<StoreInfoModel>() {}.type
                val dataGson = Gson().fromJson<StoreInfoModel>(dataJson, type)
                webView.loadUrl("$hostUrl$sendOrder/$from/${dataGson.id ?: ""}")
            } else {
                val type = object : TypeToken<MissionInfoModel>() {}.type
                val dataGson = Gson().fromJson<MissionInfoModel>(dataJson, type)
                webView.loadUrl("$hostUrl$sendOrder/$from/${dataGson.id ?: ""}")
            }
        }
    }

    private fun webSetting() {
        val webSetting = webView.settings
        webSetting.javaScriptEnabled = true
        webSetting.javaScriptCanOpenWindowsAutomatically = true

        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {

                if (newProgress == 100) {
                    progressBar4?.visibility = View.GONE
                } else {
                    progressBar4?.visibility = View.VISIBLE
                    progressBar4?.progress = newProgress
                }
            }
        }

        webView?.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (request?.url.toString().contains("success")) {
//                    if (from == "1" ) {
//                        if (dataJson.isNotEmpty()) {
//                            val bundle = Bundle()
//                            bundle.putString("storeData", dataJson)
//                            bundle.putString("from", "user")
                            val fg = StoreListFragment()
//                            fg.arguments = bundle
                            val ft = fragmentManager?.beginTransaction()
                            ft?.replace(R.id.fragment_container, fg)?.commit()
//                        } else {
//                            //沒有接收到店家資料
//                            val ft = fragmentManager?.beginTransaction()
//                            ft?.replace(R.id.fragment_container, StoreListFragment())?.commit()
//                        }
//                    } else {
//                        if (dataJson.isNotEmpty()) {
//                            val bundle = Bundle()
//                            bundle.putString("missionData", dataJson)
//                            bundle.putString("from", "user")
//                            val fg = MissionFragment()
//                            fg.arguments = bundle
//                            val ft = fragmentManager?.beginTransaction()
//                            ft?.replace(R.id.fragment_container, fg)?.commit()
//                        } else {
//                            //沒有接收到任務資料
//                            val ft = fragmentManager?.beginTransaction()
//                            ft?.replace(R.id.fragment_container, StoreListFragment())?.commit()
//                        }
//                    }
                } else {
                    view?.loadUrl(request?.url.toString())
                }
                return true
            }
        }
    }
}