package com.oversoar.slash.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.google.firebase.database.*
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Model.UserDBModel
import com.oversoar.slash.R
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.fragment_main.*

class BlockFragment: Fragment() {

    private var nAdapter: BlockAdapter? = null
    private var blockLists: ArrayList<String>? = null
    private var blockNameLists = ArrayList<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getBlockLists()
        initView()

    }

    private fun initView() {

        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.title = "黑名單管理"

        val unBlockClick:(Int)->Unit = {
            blockLists?.removeAt(it)
            blockNameLists?.removeAt(it)
            val ref = FirebaseDatabase.getInstance().getReference("/users/${userInfo?.user_id}/block")
            ref.setValue(blockLists)
            nAdapter?.updateData(blockNameLists)
        }

        nAdapter = BlockAdapter(unBlockClick)
        rv_fragment_list.adapter = nAdapter
        rv_fragment_list.addItemDecoration(DividerItemDecoration(context,DividerItemDecoration.VERTICAL))

    }

    private fun getBlockLists() {

        progressBar11?.visibility = View.VISIBLE

        val ref = FirebaseDatabase.getInstance().getReference("/users/${userInfo?.user_id}")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val userDBModel = snapshot.getValue(UserDBModel::class.java)?:return
                blockLists = userDBModel.block
                if (blockLists?.size?:0 > 0) {
                    empty_list?.visibility = View.INVISIBLE
                    getBlockUserName()
                } else {
                    progressBar11?.visibility = View.INVISIBLE
                    empty_list?.visibility = View.VISIBLE
                    empty_list?.text = "您尚未封鎖任何使用者\n（在聊天列表長壓，可選擇刪除或封鎖）"
                }
            }
        })
    }

    private fun getBlockUserName() {
        blockLists?.forEach {
            val ref = FirebaseDatabase.getInstance().getReference("/users/$it")
            ref.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    progressBar11?.visibility = View.INVISIBLE
                    val userDBModel = snapshot.getValue(UserDBModel::class.java)?:return
                    if (!blockNameLists.contains(userDBModel.user_name)) {
                        blockNameLists.add(userDBModel.user_name?:"無暱稱")
                        nAdapter?.updateData(blockNameLists)
                    }
                }
            })
        }
    }

    inner class BlockAdapter(private val unBlockClick:(Int)->Unit): RecyclerView.Adapter<BlockAdapter.ViewHolder>() {

        private var dataList: ArrayList<String>? = null

        fun updateData(data:ArrayList<String>) {
            this.dataList = data
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlockAdapter.ViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_block, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return dataList?.size?:0
        }

        override fun onBindViewHolder(viewHolder: BlockAdapter.ViewHolder, position: Int) {
            viewHolder.textView.text = dataList!![position]
            viewHolder.unblock.setOnClickListener {
                unBlockClick(position)
            }
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val textView = itemView.findViewById<TextView>(R.id.textView_item_block)
            val unblock = itemView.findViewById<TextView>(R.id.unblock_item_block)
        }
    }
}