package com.oversoar.slash

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Model.StringResultModel
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import kotlinx.android.synthetic.main.activity_captcha.*
import kotlinx.coroutines.*

class ForgetPwdActivity : AppCompatActivity() {

    private var status = 0
    private var captcha: String? = null
    private var reSend = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_captcha)

        initView()

    }

    private fun initView() {

        code_captcha.visibility = View.GONE
        textView11.visibility = View.GONE

        done_btn_captcha.setOnClickListener {
            when (status) {
                0 -> {
                    //發送驗證碼
                    verification(phone_captcha.text.toString())
                }
                1 -> {
                    //比對驗證碼
                    if (code_captcha.text.toString() == captcha) {
                        status = 2
                        code_captcha.text.clear()
                        code_captcha.hint = "輸入新密碼"
                        code_captcha.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
                        repwd_captcha.visibility = View.VISIBLE
                    } else {
                        Toast.makeText(this@ForgetPwdActivity,"驗證碼錯誤！",Toast.LENGTH_SHORT).show()
                    }
                }
                2 -> {
                    //修改密碼
                    if (code_captcha.text.toString().length >= 6 ) {
                        if (code_captcha.text.toString()
                                .isNotEmpty() && repwd_captcha.text.toString().isNotEmpty()
                        ) {
                            if (code_captcha.text.toString() == repwd_captcha.text.toString()) {
                                changePwd(phone_captcha.text.toString(),repwd_captcha.text.toString())
                            }
                        } else {
                            Toast.makeText(this@ForgetPwdActivity, "密碼與確認密碼不得為空！", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(this@ForgetPwdActivity, "密碼長度需六碼以上！", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun verification(phone:String) {
        CoroutineScope(Dispatchers.Main).launch {
            var state: ApiResult<StringResultModel>
            withContext(Dispatchers.IO) {
                state = verificationApi(phone)
            }
            when (state) {

                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success) {
                        status = 1
                        captcha = (state as ApiResult.Success).data.result
                        code_captcha.visibility = View.VISIBLE
                        Toast.makeText(this@ForgetPwdActivity, "驗證碼發送成功！", Toast.LENGTH_SHORT).show()
                        CoroutineScope(Dispatchers.Main).launch {
                            delay(60000 * 3)
                            if (status == 1) {
                                resend_captcha.visibility = View.VISIBLE
                                reSend = true
                            }
                        }
                    } else {
                        reSend = true
                        Toast.makeText(this@ForgetPwdActivity,(state as ApiResult.Success).data.msg, Toast.LENGTH_SHORT).show()
                    }
                    Log.i(
                        "CAPTCHAFragment",
                        (state as ApiResult.Success<StringResultModel>).data.toString()
                    )
                }
                is ApiResult.Error -> {
                    reSend = true
                    AlertDialog.Builder(this@ForgetPwdActivity)
                        .setTitle("訊息提示")
                        .setMessage("網路或系統有誤，請稍後再試！")
                        .show()
                }
            }
        }
    }

    private fun changePwd(phone: String,pwd:String) {
        CoroutineScope(Dispatchers.Main).launch {
            var state: ApiResult<StringResultModel>
            withContext(Dispatchers.IO) {
                state = changePwdApi(phone,pwd)
            }
            when (state) {

                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success) {
                        val sp = getSharedPreferences("userData", Context.MODE_PRIVATE)
                        sp.edit().putString("pwd",pwd).apply()
                        Commom.userInfo?.password = pwd
                        Toast.makeText(this@ForgetPwdActivity, "修改成功！", Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
                is ApiResult.Error -> {
                    AlertDialog.Builder(this@ForgetPwdActivity)
                        .setTitle("訊息提示")
                        .setMessage("網路或系統有誤，請稍後再試！")
                        .show()
                }
            }
        }
    }

    private suspend fun verificationApi (phone: String): ApiResult<StringResultModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).changePasswordVerify(
                phone)
            ApiResult.Success(result)
        } catch (e:Exception) {
            ApiResult.Error(e)
        }
    }

    private suspend fun changePwdApi (phone: String,pwd:String): ApiResult<StringResultModel>{
        return try {
            val result = ApiClient().createService(ApiService::class.java).changePassword(phone,pwd)
            ApiResult.Success(result)
        } catch (e:Exception) {
            ApiResult.Error(e)
        }
    }
}