package com.oversoar.slash.Adapter

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import com.oversoar.slash.R
import com.squareup.picasso.Picasso


class GridViewAdapter2(
    context: Context,
    var imgArray: MutableList<String>?,
    val choosePhoto: ((Int) -> Unit)?,
    val deleteClick: ((Int) -> Unit)?
) : BaseAdapter() {

    private val context = context
    private val mLayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    fun updateData(photoList: MutableList<String>) {
        this.imgArray = photoList
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return imgArray?.size?:0
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val v: View = mLayoutInflater.inflate(R.layout.item_grid_view, parent, false)
        val imageView = v.findViewById<ImageView>(R.id.iv_grid_view_store)
        val delete = v.findViewById<ImageView>(R.id.delete_gv_store)
        val textView = v.findViewById<TextView>(R.id.textView19)

        imageView.setOnClickListener {
            if (choosePhoto != null) {
                choosePhoto!!(position)
            }
        }

        delete.setOnClickListener {
            if (deleteClick != null) {
                deleteClick!!(position)
            }
        }

        if (imgArray != null) {
            if (!imgArray!![position].isNullOrEmpty()) {
                delete.visibility = View.VISIBLE
                textView.visibility = View.INVISIBLE
                Picasso.get().load(imgArray!![position]).placeholder(R.drawable.logo).fit().into(imageView)
            } else {
                delete.visibility = View.INVISIBLE
                textView.visibility = View.VISIBLE
                imageView.setImageDrawable(context.getDrawable(R.drawable.ic_add_black_24dp))
            }
        }

        return v

    }

    override fun getItem(position: Int): Any? {
        return imgArray!![position]
    }

}