package com.oversoar.slash.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.oversoar.slash.Model.MissionInfoModel
import com.oversoar.slash.Model.StoreInfoModel
import com.oversoar.slash.R
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*

class UserMissionListAdapter(
    val context: Context,
    var dataList: List<MissionInfoModel>?,
    val click:(Int)->Unit,
    val payClick:(String,Int)->Unit,
    val deleteClick:(String,Int)->Unit
) : RecyclerSwipeAdapter<UserMissionListAdapter.ViewHolder>() {

    fun updateData(dataList:List<MissionInfoModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_user_store_list, parent, false)
        return ViewHolder(view)
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        if(dataList != null) {

            viewHolder.swipeLayout.showMode = SwipeLayout.ShowMode.PullOut
            mItemManger.bindView(viewHolder.itemView, position)

            viewHolder.status.visibility = View.VISIBLE
            when (dataList!![position].states) {
                "0" -> {
                    viewHolder.status.background = context.getDrawable(R.drawable.rounded_gray_shape)
                    viewHolder.status.text = "待審核"
                }
                "1" -> {
                    //orange (待付款)
                    viewHolder.status.background = context.getDrawable(R.drawable.rounded_orange_shape)
                    viewHolder.status.text = "付款去"
                }
                "2" -> {
                    //green
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    val c = Calendar.getInstance()
                    c.time = sdf.parse(dataList!![position].expiry_date!!)!!
                    c.add(Calendar.DAY_OF_YEAR,1)
                    val now = Calendar.getInstance().time

                    if (now < c.time) {
                        viewHolder.status.background = context.getDrawable(R.drawable.rounded_green_shape)
                        viewHolder.status.text = "已付款"
                    } else {
                        viewHolder.status.background = context.getDrawable(R.drawable.rounded_orange_shape)
                        viewHolder.status.text = "已過期/付款去"
                    }
                }
                "3" -> {
                    //red
                    viewHolder.status.background = context.getDrawable(R.drawable.rounded_red_shape)
                    viewHolder.status.text = "審核未過"
                }
            }

            if (dataList!![position].image_URL_1 != "") {
                Picasso.get().load(dataList!![position].image_URL_1).placeholder(R.drawable.logo).fit().into(viewHolder.iv)
            } else {
                Picasso.get().load(R.mipmap.ic_launcher).fit().into(viewHolder.iv)
            }

            viewHolder.name.text = dataList!![position].mission_name
            viewHolder.content.text = dataList!![position].mission_detail
            viewHolder.location.text = dataList!![position].location
            viewHolder.type.text = dataList!![position].find
            viewHolder.typeDetail.text = dataList!![position].find_detail

            viewHolder.layout.setOnClickListener {
                click(position)
            }

            viewHolder.status.setOnClickListener {
                if (viewHolder.status.text.contains("付款去"))  {
                    payClick("mission", position)
                }
            }

            viewHolder.delete.setOnClickListener {
                deleteClick("mission",position)
            }

        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv = itemView.findViewById<ImageView>(R.id.iv_item_store_list)
        val name = itemView.findViewById<TextView>(R.id.name_item_store_list)
        val content = itemView.findViewById<TextView>(R.id.content_item_store_list)
        val location = itemView.findViewById<TextView>(R.id.textView)
        val type = itemView.findViewById<TextView>(R.id.textView2)
        val typeDetail = itemView.findViewById<TextView>(R.id.textView3)
        val status = itemView.findViewById<TextView>(R.id.status_item_store_list)
        val swipeLayout = itemView.findViewById<SwipeLayout>(R.id.swipe)
        val layout = itemView.findViewById<ConstraintLayout>(R.id.layout)
        val delete = itemView.findViewById<TextView>(R.id.delete)
    }
}