package com.oversoar.slash.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.oversoar.slash.Model.StoreInfoModel
import com.oversoar.slash.R
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*

class StoreListAdapter(
    val context: Context,
    val from:String,
    val click:(Int)->Unit,
    val refresh: (() -> Unit)?
) : RecyclerSwipeAdapter<StoreListAdapter.ViewHolder>() {

    private var dataList: List<StoreInfoModel>? = null
    private var page = 50

    fun updateData(dataList:List<StoreInfoModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store_list, parent, false)
        return ViewHolder(view)
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        if(dataList != null) {

            if (dataList!![position].image_URL_1 != "") {
                Picasso.get().load(dataList!![position].image_URL_1).placeholder(R.drawable.logo).fit().into(viewHolder.iv)
            } else {
                Picasso.get().load(R.mipmap.ic_launcher).fit().into(viewHolder.iv)
            }

            viewHolder.name.text = dataList!![position].store_name
            viewHolder.content.text = dataList!![position].store_detail
            viewHolder.location.text = dataList!![position].location
            viewHolder.type.text = dataList!![position].type
            viewHolder.typeDetail.text = dataList!![position].type_detail

            viewHolder.itemView.setOnClickListener {
                click(position)
            }
        }

        if (position >= page) {
            refresh ?: return
            page += 50
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv = itemView.findViewById<ImageView>(R.id.iv_item_store_list)
        val name = itemView.findViewById<TextView>(R.id.name_item_store_list)
        val content = itemView.findViewById<TextView>(R.id.content_item_store_list)
        val location = itemView.findViewById<TextView>(R.id.textView)
        val type = itemView.findViewById<TextView>(R.id.textView2)
        val typeDetail = itemView.findViewById<TextView>(R.id.textView3)
    }
}