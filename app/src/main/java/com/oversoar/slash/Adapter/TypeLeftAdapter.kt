package com.oversoar.slash.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oversoar.slash.R

class TypeLeftAdapter(
        val from:String,
        val dataList: List<String>,
        val context : Context,
        var pos:Int?,
        var choose:String,
        val click:(Int,String)->Unit
) : RecyclerView.Adapter<TypeLeftAdapter.TypeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TypeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_type, parent, false)
        return TypeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(typeViewHolder: TypeViewHolder, position: Int) {

        if (from == "type" && dataList[position] == "全部地點") {
            typeViewHolder.type.visibility = View.GONE
        } else {
            typeViewHolder.type.visibility = View.VISIBLE
        }

        typeViewHolder.type.text = dataList[position]

        typeViewHolder.type.setOnClickListener {
            pos = position
            click(position,dataList[position])
            choose = ""
            notifyDataSetChanged()
        }

        if (choose.contains(dataList[position].substring(0,2))) {
            typeViewHolder.type.setTypeface(Typeface.DEFAULT, Typeface.BOLD)
            typeViewHolder.type.background = context.getDrawable(R.color.colorWhite)
            typeViewHolder.type.setTextColor(context.getColor(R.color.colorBlack))
        } else {
            typeViewHolder.type.setTypeface(Typeface.DEFAULT,Typeface.NORMAL)
            typeViewHolder.type.background = context.getDrawable(R.color.colorGray)
            typeViewHolder.type.setTextColor(context.getColor(R.color.colorDkGray))
        }

        if (pos != null) {
            if (pos != position) {
                typeViewHolder.type.setTypeface(Typeface.DEFAULT,Typeface.NORMAL)
                typeViewHolder.type.background = context.getDrawable(R.color.colorGray)
                typeViewHolder.type.setTextColor(context.getColor(R.color.colorDkGray))
            } else {
                typeViewHolder.type.setTypeface(Typeface.DEFAULT, Typeface.BOLD)
                typeViewHolder.type.background = context.getDrawable(R.color.colorWhite)
                typeViewHolder.type.setTextColor(context.getColor(R.color.colorBlack))
            }
        } else {
            typeViewHolder.type.setTypeface(Typeface.DEFAULT,Typeface.NORMAL)
            typeViewHolder.type.background = context.getDrawable(R.color.colorGray)
            typeViewHolder.type.setTextColor(context.getColor(R.color.colorDkGray))
        }

    }

    inner class TypeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var type: TextView = itemView.findViewById(R.id.itemType)
    }

}