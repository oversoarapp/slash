package com.oversoar.slash.Adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oversoar.slash.R

class TypeRightAdapter(
    var from: String,
    var subString: Boolean,
    var dataList: List<String>?,
    val context : Context,
    var pos:Int? = null,
    var choose:String,
    val click:(Int,String)->Unit
) : RecyclerView.Adapter<TypeRightAdapter.TypeViewHolder>() {

    fun updateData(areaList:List<String>?) {
        this.dataList = areaList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TypeViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_type, parent, false)
        return TypeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(typeViewHolder: TypeViewHolder, position: Int) {

        if (dataList != null) {

            if (subString && dataList!![position].length >= 4 && dataList!![position] != "不拘地點" && dataList!![position] != "遠端作業" && dataList!![position] != "全部地點") {
                //去掉城市，只留下地區
                typeViewHolder.type.text = dataList!![position].substring(2)
            } else {
                typeViewHolder.type.text = dataList!![position]
            }

            if (from == "type") {
                if (dataList!![position].contains("全區") || dataList!![position].contains("全部")) {
                    typeViewHolder.type.visibility = View.GONE
                } else {
                    typeViewHolder.type.visibility = View.VISIBLE
                }
            }

            typeViewHolder.type.setOnClickListener {
                click(position, dataList!![position])
            }

            if (choose.contains(",")) {
                var split = choose.split(",")
                for (i in split.indices) {
                    if (dataList!![position].contains(split[i])) {
                        typeViewHolder.imageView.visibility = View.VISIBLE
                        typeViewHolder.type.setTextColor(context.getColor(R.color.colorBlack))
                        break
                    } else {
                        typeViewHolder.imageView.visibility = View.GONE
                        typeViewHolder.type.setTextColor(context.getColor(R.color.colorDkGray))
                    }
                }
            } else {
                if (!choose.contains(dataList!![position])) {
                    typeViewHolder.imageView.visibility = View.INVISIBLE
                    typeViewHolder.type.setTextColor(context.getColor(R.color.colorDkGray))
                } else {
                    typeViewHolder.imageView.visibility = View.VISIBLE
                    typeViewHolder.type.setTextColor(context.getColor(R.color.colorBlack))
                }
            }
        }
    }

    inner class TypeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var type: TextView = itemView.findViewById(R.id.itemType)
        var imageView: ImageView = itemView.findViewById(R.id.imageView)
    }
}