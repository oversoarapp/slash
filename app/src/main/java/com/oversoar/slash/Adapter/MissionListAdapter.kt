package com.oversoar.slash.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.oversoar.slash.Model.MissionInfoModel
import com.oversoar.slash.R
import com.squareup.picasso.Picasso

class MissionListAdapter(val context: Context,val from:String , val click:(Int)->Unit) : RecyclerView.Adapter<MissionListAdapter.ViewHolder>() {

    private var dataList: List<MissionInfoModel>? = null

    fun updateData(dataList:List<MissionInfoModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (dataList!![position].image_URL_1 != "") {
            Picasso.get().load(dataList!![position].image_URL_1).placeholder(R.drawable.logo).fit().into(viewHolder.iv)
        } else {
            Picasso.get().load(R.mipmap.ic_launcher).fit().into(viewHolder.iv)
        }
        viewHolder.name.text = dataList!![position].mission_name
        viewHolder.content.text = dataList!![position].mission_detail
        viewHolder.location.text = dataList!![position].location
        viewHolder.type.text = dataList!![position].find
        viewHolder.typeDetail.text = dataList!![position].find_detail

        viewHolder.itemView.setOnClickListener {
            click(position)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv = itemView.findViewById<ImageView>(R.id.iv_item_store_list)
        val name = itemView.findViewById<TextView>(R.id.name_item_store_list)
        val content = itemView.findViewById<TextView>(R.id.content_item_store_list)
        val location = itemView.findViewById<TextView>(R.id.textView)
        val type = itemView.findViewById<TextView>(R.id.textView2)
        val typeDetail = itemView.findViewById<TextView>(R.id.textView3)
    }
}