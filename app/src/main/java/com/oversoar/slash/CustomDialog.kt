package com.oversoar.slash

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Commom
import com.oversoar.slash.R
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class CustomDialog(private val _context: Context,private val id: String?,private val blockClick:((String)->Unit)?,private val deleteClick:((String)->Unit)?) : Dialog(_context) {

    init {
        setCancelable(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog)

        window?.setLayout(1000, WindowManager.LayoutParams.WRAP_CONTENT)

        val block = findViewById<TextView>(R.id.block_dialog)
        val delete = findViewById<TextView>(R.id.delete_dialog)

        block.setOnClickListener {
            blockClick!!(id?:"")
        }

        delete.setOnClickListener {
            deleteClick!!(id?:"")
        }

    }
}