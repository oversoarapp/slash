package com.oversoar.slash.Remote

import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Model.UserModel

class LoginApi {

    suspend fun login (acc:String, pwd:String, type:String, token:String): ApiResult<UserModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).login(acc,pwd,type,token)
            ApiResult.Success(result)
        } catch (e:Exception) {
            ApiResult.Error(e)
        }
    }

}