package com.oversoar.slash.Remote

import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Model.*
import com.oversoar.slash.Remote.ApiUrl.changeMissionStates
import com.oversoar.slash.Remote.ApiUrl.changePassword
import com.oversoar.slash.Remote.ApiUrl.changePasswordVerify
import com.oversoar.slash.Remote.ApiUrl.changeStoreStates
import com.oversoar.slash.Remote.ApiUrl.changeUserName
import com.oversoar.slash.Remote.ApiUrl.deleteMission
import com.oversoar.slash.Remote.ApiUrl.deleteStore
import com.oversoar.slash.Remote.ApiUrl.followMission
import com.oversoar.slash.Remote.ApiUrl.followMissionList
import com.oversoar.slash.Remote.ApiUrl.followStore
import com.oversoar.slash.Remote.ApiUrl.followStoreList
import com.oversoar.slash.Remote.ApiUrl.getMissionData
import com.oversoar.slash.Remote.ApiUrl.getStoreData
import com.oversoar.slash.Remote.ApiUrl.getUserMission
import com.oversoar.slash.Remote.ApiUrl.getUserStore
import com.oversoar.slash.Remote.ApiUrl.hostUrl
import com.oversoar.slash.Remote.ApiUrl.login
import com.oversoar.slash.Remote.ApiUrl.register
import com.oversoar.slash.Remote.ApiUrl.saveMission
import com.oversoar.slash.Remote.ApiUrl.saveStore
import com.oversoar.slash.Remote.ApiUrl.sendMail
import com.oversoar.slash.Remote.ApiUrl.sendMsg
import com.oversoar.slash.Remote.ApiUrl.uploadMissionImg
import com.oversoar.slash.Remote.ApiUrl.uploadStoreImg
import com.oversoar.slash.Remote.ApiUrl.uploadUserImage
import com.oversoar.slash.Remote.ApiUrl.verification
import retrofit2.http.*

interface ApiService {
    @POST(hostUrl+ login)
    suspend fun login(@Query("account")account:String,
                      @Query("password")password:String,
                      @Query("type")type:String,
                      @Query("FCM_token")FCM_token:String
    ): UserModel

    @POST(hostUrl+ register)
    suspend fun signUp(@Query("type")type:String,
                      @Query("user_name")user_name:String,
                      @Query("phone")phone:String,
                      @Query("password")password:String,
                      @Query("image_URL")image_URL:String,
                      @Query("email")email:String,
                      @Query("FCM_token")FCM_token:String
    ): UserModel

    @POST(hostUrl+ verification)
    suspend fun verification (@Query("phone")phone:String): StringResultModel

    @POST(hostUrl+ changePasswordVerify)
    suspend fun changePasswordVerify (@Query("phone")phone:String): StringResultModel

    @POST(hostUrl+ changePassword)
    suspend fun changePassword (@Query("phone")phone:String,
                                @Query("password")password:String
    ): StringResultModel

    @POST(hostUrl+ sendMsg)
    suspend fun sendMsg (@Query("user_name")user_name:String,
                         @Query("text")text:String,
                         @Query("target_id")target_id:String
    )

    @POST(hostUrl+ sendMail)
    suspend fun sendMail (@Query("userEmail")userEmail:String,
                         @Query("content")content:String,
                         @Query("name")name:String,
                         @Query("title")title:String
    ):UserModel

    @POST(hostUrl+ changeUserName)
    suspend fun changeUserName (@Query("user_id")user_id:String,
                                @Query("user_name")user_name:String
    )

    @POST(hostUrl+ getUserStore)
    suspend fun getUserStore (@Query("user_id")user_id:String): StoreModel

    @POST(hostUrl+ getUserMission)
    suspend fun getUserMission (@Query("user_id")user_id:String): MissionModel

    @POST(hostUrl+ saveStore )
    suspend fun saveStore (@Query("store_id")store_id:String,
                           @Query("store_name")store_name:String,
                           @Query("store_detail")store_detail:String,
                           @Query("location")location:String,
                           @Query("type")type:String,
                           @Query("type_detail")type_detail:String,
                           @Query("user_id")user_id:String
    ):SingleStoreModel

    @POST(hostUrl+ saveMission )
    suspend fun saveMission (@Query("mission_id")mission_id:String,
                           @Query("mission_name")mission_name:String,
                           @Query("mission_detail")mission_detail:String,
                           @Query("location")location:String,
                           @Query("find")find:String,
                           @Query("find_detail")find_detail:String,
                           @Query("user_id")user_id:String
    ):SingleMissionModel

//    @FormUrlEncoded
    @POST(hostUrl+ uploadStoreImg )
    suspend fun uploadStoreImg (@Query("store_id")store_id:String,
                           @Query("user_id")user_id:String,
                           @Query("order")order:Int,
                           @Query("url")url:String
    )

    @POST(hostUrl+ uploadMissionImg)
    suspend fun uploadMissionImg (@Query("mission_id")mission_id:String,
                                @Query("user_id")user_id:String,
                                @Query("order")order:Int,
                                @Query("url")url:String
    )

//    @FormUrlEncoded
    @POST(hostUrl+ uploadUserImage )
    suspend fun uploadUserImage (@Query("user_id")user_id:String,
                                @Query("url")url:String
    )

    @POST(hostUrl+ changeStoreStates )
    suspend fun updateStoreStates (@Query("id")id:String,
                                 @Query("states")states:String
    ):String

    @POST(hostUrl+ changeMissionStates )
    suspend fun updateMissionStates (@Query("id")id:String,
                                   @Query("states")states:String
    ):String

    @POST(hostUrl+ followStoreList )
    suspend fun followStoreList (@Query("user_id")user_id:String):StoreModel

    @POST(hostUrl+ followMissionList )
    suspend fun followMissionList (@Query("user_id")user_id:String):MissionModel

    @POST(hostUrl+ followStore )
    suspend fun followStore (@Query("store_id")store_id:String,
                             @Query("user_id")user_id:String,
                             @Query("action")action:String
    ):StoreModel

    @POST(hostUrl+ followMission )
    suspend fun followMission (@Query("user_id")user_id:String,
                               @Query("mission_id")mission_id:String,
                               @Query("action")action:String
    ):MissionModel

    @FormUrlEncoded
    @POST(hostUrl+ getStoreData)
    suspend fun getStoreData (@Query("search")search:String,
                              @Query("page")page:String,
                              @Field("location[]")location:List<String>,
                              @Query("type")type:String,
                              @Field("type_detail[]")type_detail:List<String>
    ): StoreModel

    @POST(hostUrl+ getMissionData)
    suspend fun getMissionData (@Query("search")search:String,
                              @Query("page")page:String,
                              @Query("location[]")location:List<String>,
                              @Query("find")type:String,
                              @Query("find_detail[]")type_detail:List<String>
    ): MissionModel

    @POST(hostUrl+ deleteStore)
    suspend fun deleteStore (@Query("user_id")user_id:String,
                             @Query("store_id")store_id:String
    ): StoreModel

    @POST(hostUrl+ deleteMission)
    suspend fun deleteMission (@Query("user_id")user_id:String,
                               @Query("mission_id")mission_id:String
    ): MissionModel

}