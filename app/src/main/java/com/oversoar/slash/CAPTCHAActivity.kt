package com.oversoar.slash

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Commom.fcmToken
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Model.UserDBModel
import com.oversoar.slash.Model.UserModel
import com.oversoar.slash.Model.StringResultModel
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import kotlinx.android.synthetic.main.activity_captcha.*
import kotlinx.coroutines.*

class CAPTCHAActivity : AppCompatActivity() {

    companion object {
        val USERACC = "acc"
        val USERPWD = "pwd"
        val SIGNUPTYPE = "type"
        val USERNAME = "name"
        val USERIMGURL = "imgUrl"
        val USEREMAIL = "email"
    }

    private val storage = Firebase.storage
    private var captcha: String? = null
    private var acc = ""
    private var type = ""
    private var reSend = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_captcha)

        initView()

    }

    private fun initView() {

        supportActionBar?.title = "手機號碼驗證"
        type = intent.getStringExtra(SIGNUPTYPE)?:""
        acc = intent.getStringExtra(USERACC)?:""
        if (type == "0") {
            phone_captcha.setText(acc)
            phone_captcha.isEnabled = false
            check_phone_captcha.visibility = View.VISIBLE
        }

        phone_captcha.addTextChangedListener(PhoneTextWatcher())

        check_phone_captcha.setOnClickListener {
            when (check_phone_captcha.text) {
                "發送驗證碼" -> {
                    verification(phone_captcha.text.toString())
                }
                "再次發送驗證碼" -> {
                    if (reSend) {
                        reSend = false
                        verification(phone_captcha.text.toString())
                    } else {
                        Toast.makeText(this, "3分鐘後才可再次發送驗證碼！", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }

        done_btn_captcha.setOnClickListener {
            if ( captcha != null && code_captcha.text.toString() == captcha) {
                val type = intent.getStringExtra(SIGNUPTYPE)?:""
                val phone = phone_captcha.text.toString()
                val pwd = intent.getStringExtra(USERPWD)?:""
                val name = intent.getStringExtra(USERNAME)?:""
                val email = intent.getStringExtra(USEREMAIL)?:""
                val url = intent.getStringExtra(USERIMGURL)?:""
                signUp(type,name,phone,pwd,url,email,fcmToken)
            } else {
                Toast.makeText(this,"驗證碼錯誤！",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun saveToFirebase(pwd:String) {

        if (type == "0") {
            val img = intent.getByteArrayExtra(USERIMGURL) ?: ByteArray(0)
            if (img.isNotEmpty()) {
                uploadImg()
            }
        }

        val ref = FirebaseDatabase.getInstance().getReference("/users/${userInfo?.user_id}")
        val model = UserDBModel(
            userInfo?.user_id ?: "",
            userInfo?.user_name ?: "",
            userInfo?.image_URL ?: "",
                arrayListOf()
        )

        ref.setValue(model)
            .addOnSuccessListener {

                val sp = getSharedPreferences("userData", Context.MODE_PRIVATE)
                sp.edit().putString("acc",acc).apply()
                sp.edit().putString("pwd",pwd).apply()

                val intent = Intent(this,MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                Log.i("","saveUserToDB Successfully")
            }
            .addOnFailureListener {
                Toast.makeText(this,"error",Toast.LENGTH_SHORT).show()
                val intent = Intent(this,MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                Log.i("","saveUserToDB Failed: $it")
            }
    }

    private fun uploadImg () {

        val ref = storage.reference.child("user${userInfo?.user_id?:""}/user.jpg")
        val uploadTask = ref.putBytes(intent?.getByteArrayExtra(USERIMGURL)!!)

        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            ref.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                userInfo?.image_URL = task.result.toString()
                CoroutineScope(Dispatchers.IO).launch {
                    ApiClient().createService(ApiService::class.java)
                        .uploadUserImage(
                            userInfo?.user_id ?: "",
                            userInfo?.image_URL?:""
                        )
                }
            }
        }
    }

    inner class PhoneTextWatcher: android.text.TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (s.toString().length ==10 ) {
                check_phone_captcha.visibility = View.VISIBLE
            } else {
                check_phone_captcha.visibility = View.GONE
            }
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

    }

    private fun signUp (type:String, name:String, phone:String,pwd:String, imgUrl:String,email:String ,token:String) {

        progressBar7.visibility = View.VISIBLE

        CoroutineScope(Dispatchers.Main).launch {
            var state: ApiResult<UserModel>
            withContext(Dispatchers.IO) {
                state = signUpApi(type,name,phone,pwd,imgUrl,email,token)
            }
            when (state) {
                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success) {
                        userInfo = (state as ApiResult.Success).data.result
                        saveToFirebase(pwd)
                    } else {
                        //api失敗
                        Toast.makeText(this@CAPTCHAActivity, (state as ApiResult.Success).data.msg, Toast.LENGTH_SHORT).show()
                        Log.i("Failure", (state as ApiResult.Success).data.msg)
                    }
                }
                is ApiResult.Error -> {
                    AlertDialog.Builder(this@CAPTCHAActivity)
                        .setTitle("訊息提示")
                        .setMessage("網路或系統有誤，請稍後再試！")
                        .show()
                    Log.e("ERROR", Log.getStackTraceString((state as ApiResult.Error).exception))
                }
            }
        }
    }

    private suspend fun signUpApi (type:String, name:String, phone:String, pwd:String, imgUrl:String,email:String ,token:String): ApiResult<UserModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).signUp(type,name,phone,pwd,imgUrl,email,token)
            ApiResult.Success(result)
        } catch (e:Exception) {
            ApiResult.Error(e)
        }
    }

    private fun verification(phone:String) {
        CoroutineScope(Dispatchers.Main).launch {
            var state: ApiResult<StringResultModel>
            withContext(Dispatchers.IO) {
                state = verificationApi(phone)
            }
            when (state) {

                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success) {
                        captcha = (state as ApiResult.Success).data.result
                        Toast.makeText(this@CAPTCHAActivity, "驗證碼發送成功！", Toast.LENGTH_SHORT).show()
                        check_phone_captcha.text = "再次發送驗證碼"
                        CoroutineScope(Dispatchers.Main).launch{
                            delay(60000*3)
                            reSend = true
                        }
                    } else {
                        reSend = true
                        Toast.makeText(this@CAPTCHAActivity, (state as ApiResult.Success<StringResultModel>).data.msg, Toast.LENGTH_SHORT).show()
                    }
                    Log.i("captchaCode",(state as ApiResult.Success).data.toString())
                }
                is ApiResult.Error -> {
                    reSend = true
                    AlertDialog.Builder(this@CAPTCHAActivity)
                        .setTitle("訊息提示")
                        .setMessage("網路或系統有誤，請稍後再試！")
                        .show()
                }
            }
        }
    }

    private suspend fun verificationApi (phone:String): ApiResult<StringResultModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).verification(phone)
            ApiResult.Success(result)
        } catch (e:Exception) {
            ApiResult.Error(e)
        }
    }
}