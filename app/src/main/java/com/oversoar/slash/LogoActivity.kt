package com.oversoar.slash

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.oversoar.madolsss.remote.ApiResult
import com.oversoar.slash.Commom.fcmToken
import com.oversoar.slash.Commom.userInfo
import com.oversoar.slash.Commom.userMissionData
import com.oversoar.slash.Commom.userMissionFollow
import com.oversoar.slash.Commom.userStoreData
import com.oversoar.slash.Commom.userStoreFollow
import com.oversoar.slash.Model.MissionModel
import com.oversoar.slash.Model.StoreModel
import com.oversoar.slash.Model.UserModel
import com.oversoar.slash.Remote.ApiClient
import com.oversoar.slash.Remote.ApiService
import com.oversoar.slash.Remote.LoginApi
import kotlinx.android.synthetic.main.activity_logo.*
import kotlinx.coroutines.*

class LogoActivity : AppCompatActivity() {

    private var sp: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logo)

        initView()
        checkTermsStatus()

    }

    private fun initView() {

        supportActionBar?.title = "使用者條款"
        supportActionBar?.show()
        textView4.movementMethod = ScrollingMovementMethod.getInstance()

        sp = getSharedPreferences("userData", Context.MODE_PRIVATE)

        btn_logo.setOnClickListener {
            sp?.edit()?.putBoolean("terms",true)?.apply()
            terms_view_logo.visibility = View.GONE
            logo_iv_logo.visibility = View.VISIBLE
            getToken()
        }

    }

    private fun checkTermsStatus() {
        if (sp?.getBoolean("terms",false) == false ) {
            terms_view_logo.visibility = View.VISIBLE
            logo_iv_logo.visibility = View.GONE
        } else {
            supportActionBar?.hide()
            getToken()
        }
    }

    private fun checkLogin() {
        if (sp?.getString("acc", "")?.isEmpty() == true) {
            CoroutineScope (Dispatchers.Main).launch {
                delay(3000)
                val intent = Intent(this@LogoActivity, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK))
                startActivity(intent)
            }
        } else {
            login()
        }
    }

    private fun getToken() {
        FirebaseMessaging.getInstance().subscribeToTopic("global")
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) { instanceIdResult ->
            fcmToken = instanceIdResult.token
            checkLogin()
            Log.i("FCMToken", fcmToken)
        }
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
    }

    private fun login() {
        val acc = sp?.getString("acc", "") ?: ""
        val pwd = sp?.getString("pwd", "") ?: ""

        CoroutineScope(Dispatchers.Main).launch {
            var state: ApiResult<UserModel>
            withContext(Dispatchers.IO) {
                state = LoginApi()
                    .login(acc, pwd, "0", fcmToken)
            }

            when (state) {
                is ApiResult.Success -> {
                    if ((state as ApiResult.Success).data.success) {
                        //有response
                        userInfo = (state as ApiResult.Success).data.result
                        userStoreFollow = (state as ApiResult.Success).data.store_follows
                        userMissionFollow = (state as ApiResult.Success).data.mission_follows
                        getData()
                    }
                }
                is ApiResult.Error -> {
                    val intent = Intent(this@LogoActivity, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK))
                    startActivity(intent)
                    Log.e("ERROR", Log.getStackTraceString((state as ApiResult.Error).exception))
                }
            }
        }
    }

    private fun getData() {
        CoroutineScope(Dispatchers.Main).launch {
            var storeResult: ApiResult<StoreModel>
            var missionResult: ApiResult<MissionModel>
            withContext(Dispatchers.IO) {
                storeResult = userStoreData(userInfo?.user_id ?: "")
                missionResult = userMissionData(userInfo?.user_id ?: "")
            }
            when (storeResult) {
                is ApiResult.Success -> {
                    if ((storeResult as ApiResult.Success<StoreModel>).data.success) {
                        if (!(storeResult as ApiResult.Success<StoreModel>).data.result.isNullOrEmpty()) {
                            userStoreData = (storeResult as ApiResult.Success<StoreModel>).data.result
                            Log.i("storeData", (storeResult as ApiResult.Success<StoreModel>).data.toString())
                        }
                    }
                }

                is ApiResult.Error -> {
                    Log.i("storeData", (storeResult as ApiResult.Error).exception.toString())
                }
            }

            when (missionResult) {
                is ApiResult.Success -> {
                    if ((missionResult as ApiResult.Success<MissionModel>).data.success == true) {
                        if (!(missionResult as ApiResult.Success<MissionModel>).data.result.isNullOrEmpty()) {
                            userMissionData = (missionResult as ApiResult.Success<MissionModel>).data.result!!
                        }
                    }

                    Log.i("missionData", (missionResult as ApiResult.Success<MissionModel>).data.toString())

                }

                is ApiResult.Error -> {
                    Log.i("missionData", (missionResult as ApiResult.Error).exception.toString())
                }
            }

            val intent = Intent(this@LogoActivity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            startActivity(intent)

        }
    }

    private suspend fun userStoreData(id:String): ApiResult<StoreModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getUserStore(id)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }

    private suspend fun userMissionData(id:String): ApiResult<MissionModel> {
        return try {
            val result = ApiClient().createService(ApiService::class.java).getUserMission(id)
            ApiResult.Success(result)
        } catch (e: Throwable) {
            ApiResult.Error(e)
        }
    }
}